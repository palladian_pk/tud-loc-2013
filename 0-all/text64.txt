Italian president rules out new vote as parties wrangle

(Reuters) - President Giorgio Napolitano ruled out an early return to the polls on Friday as <COUNTRY role="main">Italy</COUNTRY>'s parties wrangled over how to form a government after this week's deadlocked election.

Speaking during a state visit to <CITY>Berlin</CITY>, Napolitano said <COUNTRY>Italy</COUNTRY> needed a stable government and could not immediately hold a new election.

"I'm not interested in going back to vote again," he told reporters at the margins of an event at the <POI>Humboldt University</POI>.

Napolitano's mandate ends in mid-May but he said his successor would be just as reluctant to call a new vote.

"I doubt that a new president will be thinking only of new elections. We'll have to see how to give <COUNTRY>Italy</COUNTRY> a government," the head of state said.

He made his comments as the three main blocs in parliament grappled with the aftermath of a vote that has left none with a workable majority and has revived fears of a return to the euro zone debt crisis.

Economic data on Friday underlined the extent of the problems a new government will face, with youth unemployment rising to a record of almost 39 percent and public debt at 127 percent of gross domestic product.

Democratic Party (PD) leader Pier Luigi Bersani, whose center-left coalition has a lower house majority but not enough seats to control the Senate, ruled out a "grand coalition" with Silvio Berlusconi's center right.

"I want to spell it out clearly: the idea of a grand coalition does not exist and will never exist," he told the daily La Repubblica in an interview on Friday.

This shut off one of two apparent options for a new government, by closing the door on a formal alliance between the two biggest parties - which both backed the technocrat government of outgoing Prime Minister Mario Monti.

On Friday Berlusconi appeared in court at one of the three trials he is currently facing and denied allegations of tax fraud. He has also rejected separate accusations that he paid bribes to bring down <COUNTRY>Italy</COUNTRY>'s last center-left government in 2006.

The legal cases are one of the reasons there is huge opposition in Bersani's party to any alliance with the scandal-plagued media baron. Rank-and-file members believe any alliance with Berlusconi would bleed even more of their support to the anti-establishment party of Beppe Grillo.

Grillo's 5-Star Movement, which rode a huge protest vote to become <COUNTRY>Italy</COUNTRY>'s third force in the election, has ruled out giving a vote of confidence to another party but says it may back individual laws.

In a sign of the tensions, the comic and blogger accused the PD of trying to persuade a number of 5-Star members to support a center-left government.

"The 5-Star Movement, its deputies, its activists and its voters are not for sale. Bersani is finished and he doesn't realize it," he said in a blog post.

Grillo, whose Internet-based movement has shaken up Italian politics, has described the 61-year-old former industry minister as a "dead man talking" and said the next government will not last more than a year.

AUSTERITY

Bersani has refused to resign despite the center left throwing away a 10-point opinion poll lead with a weak campaign that let both Berlusconi and Grillo exploit public anger over the economy to make huge strides in the weeks before the vote.

But he is under pressure and there is speculation he could be replaced, possibly by Matteo Renzi, the dynamic young mayor of <CITY>Florence</CITY> whom he defeated in last year's primary to select the leader of the center-left coalition.

Without majority support in both houses of parliament, a government cannot pass legislation or win a vote of confidence and the deadlock between the parties has left it completely unclear how a new administration could be formed.

Bersani told La Repubblica he would present a program based around a limited number of points, many of which are in line with Grillo's platform, and seek the support of parliament.

"You can call it what you want, a minority government, a government of limited purpose, I don't care," he said.

He said he would also present measures to strengthen the welfare system, cut the size of <COUNTRY>Italy</COUNTRY>'s bloated parliament and reduce the generous salaries and benefits of deputies, among the best paid in <CONTINENT>Europe</CONTINENT>.

In addition, he would propose a series of measures to fight corruption and limit conflicts of interest for politicians.

Bersani said he would seek to ease the austerity programs imposed by Monti with the approval of the European Union, saying he had consulted French Socialist President Francois Hollande. He added: "Austerity on its own leads to disaster."

"Everyone has to get it into their head that bringing down the debt and the deficit is an issue which has to be shifted to the medium term," he said. "At the moment, there is another priority, which is jobs."

After an initial sell-off, financial markets have so far taken the instability largely in their stride, but the deadlock has raised fears the euro zone debt crisis, which brought Monti to power in 2011, could flare up again.

<COUNTRY>Italy</COUNTRY> has pledged to balance its budget in structural, or growth-adjusted terms, this year but the austerity measures imposed by Monti to reach the goal have pushed it deeper into recession.

Data from statistics agency Istat showed that even in terms of budget policy, the tax hikes and spending cuts imposed by Monti's technocrat government had limited effect, with <COUNTRY>Italy</COUNTRY> only just scraping within European Union limits.