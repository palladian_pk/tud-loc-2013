<COUNTRY>Poland</COUNTRY>: Five free things to do in <CITY role="main">Gdansk</CITY>

<CITY>Gdansk</CITY> offers a chance for visitors to take in scenes of sea, ships and spires set in a magnificently reconstructed Old Town while contemplating the ups and downs of European history, including the famous shipyard strike led by Lech Walesa.Already one of <COUNTRY>Poland</COUNTRY>'s key tourist attractions, <CITY>Gdansk</CITY> experienced an influx of soccer fans and tourists in June when <CITY>Gdansk</CITY> hosted several games during the European football championship. The major sporting event took take place across eight cities in <COUNTRY>Poland</COUNTRY> and <COUNTRY>Ukraine</COUNTRY>.

Here are five things that visitors to <CITY>Gdansk</CITY> can do for free:

MAIN TOWN: The colourful facades of stately merchant homes dating back centuries attest to the prosperity of the Renaissance era, when <CITY>Gdansk</CITY> was a major European hub for trade. A 17th century fountain depicting Neptune, the god of the sea, sits in the heart of the main square, <POI>Dlugi Targ</POI> (<POI>Long Market</POI>), and has become a city symbol. The historic centre is not large but contains many architectural gems meticulously reconstructed after the devastation of World War II.

<LANDMARK>WESTERPLATTE</LANDMARK>: World War II began in <CITY>Gdansk</CITY>, and today many visitors flock to the peninsula of <LANDMARK>Westerplatte</LANDMARK>, where the Germans opened fired on a Polish garrison on 1 September 1939, some of the opening shots of the conflict. A monument honours the Poles who fought for seven days against the German artillery attack.

WALESA'S SHIPYARD: Decades after the war, <CITY>Gdansk</CITY> again became a focal point of European history when Lech Walesa led a workers' strike at the shipyard in <CITY>Gdansk</CITY>.

The freedom movement he founded, Solidarity, went on to play a key role in toppling communism across the East Bloc.

Outside the shipyard is a soaring monument of three crosses in steel that honours workers killed in an earlier workers' revolt in 1970. Inside a museum depicts the rise of Solidarity and the hardship of life under communism but there is an entry fee, about $2.

<STREET>MARIACKA STREET</STREET>: This small street in the Old Town is lined with shop after shop selling amber jewellery and decorative items. Some of the buildings are decorated with stone gargoyles, adding to the lane's romantic and mysterious atmosphere. Shoppers can also browse stores selling silver, antiques and art on this and neighbouring streets.

<POI>MOTLAWA WATERFRONT</POI>: The marina of the <LANDMARK>Motlawa River</LANDMARK> is a great place for a stroll. Ships dock in the water, with bustling cafes and shops with more amber and trinkets creating a lively atmosphere in the shadow of more historic structures. One of them is a 15th century harbour crane, a massive brick structure with huge ropes and gears