Kia ora: <CITY role="main">Geraldine</CITY>

Elisabeth Easther on a charming southern town
 
Origin of name: Original Maori name <CITY>Heretini</CITY>. Europeans renamed it <CITY>Talbot Forest</CITY>, then Fitzgerald after the first superintendent of <CITY>Canterbury</CITY>, then <CITY>Geraldine</CITY> for Fitzgerald's family name in <COUNTRY>Ireland</COUNTRY>.

Population: 2244 (2006 census), 4600 in the district.

Where is it: An hour-and-a-half from <POI>Christchurch Airport</POI> (140km).

The town slogan: Easy to find - hard to leave.

The town mascot: There isn't an official one, but Pole People are popping up all over the district. Go to victorianwoodworks.co.nz to see for yourself.

Interesting historical fact: It took only 25 years to cut down almost all of the native forest in the district, to provide the timber to build much of <CITY>Timaru</CITY>, <CITY>Ashburton</CITY> and <CITY>Christchurch</CITY>.

Main industries: Barkers of Geraldine - jams, chutneys and fruit drinks since 1969. Also farming and tourism.

Source of pride: The caring community.

Town competition: The Christmas Parade Float Competition, which is very competitive with more than 50 floats each year.

Best reason to stop: Barkers, <POI>Talbot Forest Cheese</POI> and the fabulous cafes and shops.

Best place to take the kids: The river gorges - <LANDMARK>Te Moana</LANDMARK> and <LANDMARK>Orari</LANDMARK> have great holes to swim in, and waterfalls to jump from. Also The <LANDMARK>Farmyard Holiday Park</LANDMARK> - stay there or just visit; there are dozens of animals kids can actually play with.

Best drink: <POI>The Village Inn</POI> - sit on the village green in the centre of town and watch the world go by.

Best food: <POI>The Brewery Cafe</POI>. Great food, boutique beer and local wines.

Most famous locals: Annabel Ritchie, the rower, was born here, as was musician Jordan Luck, who wrote a song about the place. Rally driver Hayden Paddon, former MP Philip Burdon, and artist Austen Deans.

Best flat white: <POI>Verde Cafe</POI> and <POI>Deli</POI>.

Best bakery: <POI>The Berry Barn</POI>.

Best local website: gogeraldine.co.nz

Best museum: <POI>Geraldine Museum</POI>. There's also the very fine <POI>Vintage Car and Machinery Museum</POI> and, for art, the <POI>McAtamney Gallery</POI> has ever-changing exhibitions.

Best walk:<LANDMARK>Talbot Forest</LANDMARK>, a unique lowland podocarp forest only five minutes from town centre. And, in nearby <LANDMARK>Peel Forest</LANDMARK>, the <POI>Big Tree Walk</POI> is a 30-minute round trip, leading into Mills Bush. Filled with fine specimens of huge totara, one tree is almost 3m across and thought to be about 1000 years old.

Best place to pull over: Beside <LANDMARK>Waihi River</LANDMARK>; take the River Walk, through <LANDMARK>Rhododendron Dell</LANDMARK>.

Best facilities: Near the museum in the centre of town.

Best park/playground: <LANDMARK>Geraldine Domain</LANDMARK> - there's a swimming pool, slides, swings, picnic area, and even an old tractor to climb on.

Here for a short time: <POI>Visit The Giant Jersey</POI> - the world's largest jersey, on <STREET>Wilson St</STREET>.

Best shop: <POI>Coco</POI>, the chocolate shop - one of three chocolate shops in town. That's my kind of town.

Best swim: Either in <LANDMARK>Te Moana River</LANDMARK> or <LANDMARK>Orari River</LANDMARK> - both have dozens of swimming holes.

Unique to <CITY>Geraldine</CITY>: The Medieval Mosaic is a copy of the Bayeux tapestry. Made from thousands of tiny metal cogs, it depicts the history of the Battle of Hastings.

Interesting item of wildlife: The very rare pekapeka, otherwise known as the long-tail bat.

When a local has visitors from abroad staying ... they take them on adventures like white-water rafting, kayaking, skydiving or biking. Or salmon and fly-fishing. Or to the four nearby ski-fields - <POI>Mt Hutt</POI>, <POI>Mt Dobson</POI>, <POI>Round Hill</POI> (<POI>Tekapo</POI>) and <POI>Fox Peak</POI>. An hour away, there's also ice-skating and hot-pools. For more sedentary guests, there's the Medieval Mosaic and the <POI>Geraldine Vintage Car Museum</POI>.

Never make the mistake of ... thinking half an hour is long enough to stop in <CITY>Geraldine</CITY>.

Visitors say: We so wish we had known what a fantastic place <CITY>Geraldine</CITY> is - we would have stayed longer.

Locals say: We know what a fantastic place <CITY>Geraldine</CITY> is - that's why we live here.

BEST VIEW

Drive up on to <POI>The Downs</POI> to see <POI>Mt Peel</POI>, <POI>Mt Hutt</POI> and along the <LANDMARK>Southern Alps</LANDMARK>. Or drive for 40 minutes along the road to <REGION>Mesopotamia</REGION>, into the middle of the <LANDMARK>Southern Alps</LANDMARK>, and the <LANDMARK>Rangitata River</LANDMARK> and majestic mountains will suddenly appear suddenly before you.

BEST-KEPT SECRET

<CITY>Geraldine</CITY> is the least windy town in <COUNTRY>New Zealand</COUNTRY> (according to Jim Hickey).