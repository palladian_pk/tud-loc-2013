Obviously, I’d Come For The Sex

I was obviously there for the sex, at least according to the taxi driver who showed me around <CITY role="main">Medellín</CITY> the first day of my first visit to this city back in early 2010.

That wasn’t the reality, but I understood the confusion. <CITY>Medellín</CITY> has a reputation as a sex destination. Prostitution is legal here, and Colombian women are beautiful. It’s these stereotypes that were, at the time, attracting a lot of guys my age to this city–the promise of a beautiful Colombian wife…or, well, sometimes, a shorter-term relationship with one of the city’s beautiful women.

“If you were younger, it would be drugs,” the taxisa went on to explain that first day I toured around. “The young ones want drugs…the older ones want women.”

He wasn’t the only person I heard this theory from. A female real estate agent told me the same thing, as did my attorney.

But that was the “old” <CITY>Medellín</CITY>, of just three years ago. A lot has changed since then. Let’s turn the clock ahead to today, 2013.

On my most recent flight from <CITY>Miami</CITY> to <CITY>Medellín</CITY> about a month ago, the makeup of Americans on the plane was noticeably different. In addition to the single guys I’ve come to take for granted, I saw two groups of young women coming down for vacation, young people with kids, and a handful of “over-50″ couples.

Every Sunday, the main avenue through <CITY>Medellín</CITY>’s <UNIT>El Poblado</UNIT> is closed to vehicular traffic and fills up with people enjoying the brilliant weather, strolling along the avenue, biking, jogging, walking their dogs. And, week after week, I continue to be amazed by the number of American couples with school-aged children I now notice among these Sunday strollers.

And just yesterday, I collaborated with an American couple at the wine shop, trying to find the best value among the Chilean wines on offer.

Membership at our gym now includes a number of American couples…and a new couple joined just this morning.

While walking around town, my wife Julie and I have seen dozens of retirement-aged North American couples exploring the area and a few pairs of 50-something women. Some we’ve gotten to know. And everyone seems amazed by the terrific quality of life available here.

So the <CITY>Medellín</CITY> of 2013 is quite unlike the <CITY>Medellín</CITY> of 2010, even though 2010 wasn’t that long ago.

Expat spots around the world go through a fairly predictable cycle, morphing through a number of stages between their initial discovery and their maturity as a mainstream destination. The specifics of this evolution vary, but the general principles remain the same, as follows:

Phase I, the Adventurer Phase: During this phase, the destination attracts people who will go anywhere, regardless of existing stereotypes or perceived danger. These include adventurers and backpackers, as well sex travelers and those hiding from ex-spouses, pending lawsuits, etc.

Phase II, the Early Investor Phase: During this phase, people with money to invest, who believe they see potential in the destination, begin to visit and to take positions. At this stage, there is little to indicate that the destination will boom, so these early investors are acting on their instincts and past experience. At the Early Investor stage, alternative publications are beginning to sound the alarm that a destination of note is emerging.

Phase III, the Early Adopter Phase: During this stage, more investors come to get involved, as well as leading-edge second home buyers, overseas retirees, and those who want to work from abroad or start a business. At this point, the expats who show up are able and willing to adapt to the culture, learn speak the language, and fit in.

Generally, properties continue to qualify as bargain during these first three phases.

Phase IV, the Mainstream Buyer Phase: During this phase, the expat community is beginning to form a “critical mass” and to take on an identity of its own. By this point, some expats have blended into the local community, while others can begin to survive within the expat community alone. Phase 4 also brings opportunities for more entrepreneurs, who can count on a fair share of expat business for their survival. Real estate prices during this stage move from bargain levels at the beginning to what I’d call “fairly priced” as the stage progresses. Early investors from phases I and II–at least those in it for the investment only–are thinking about moving on.

Phase V, the Expat Saturation Phase: Here the expat community can take on a life of its own and becomes a recognized cultural entity within the local environment. This phase is also characterized by loads of opportunities for expat entrepreneurs catering to other expats. Newcomers can begin to survive on their own, without learning the language or ever becoming part of the local community. During this phase, property prices can reach premium levels. Early Investors may sell at this point, and Early Adopters from Phase III probably begin to miss the adventure of those earlier times.

During Phases I through early Phase IV, most expats are adaptors and survivors, and most succeed in achieving the lifestyle they came looking for. During late-Phase 4 and Phase 5, the expats moving in are increasingly what I would describe as just “along for the ride.” They didn’t do much investigating or preparing and aren’t really willing to adapt. These folks can become resentful and unhappy.

My friend Rich Holman, founder of First American Realty Medellín, was an exception to this “phase” model. Rich moved to <CITY>Medellín</CITY> and started a business squarely in Phase 1. When I met him in 2010, he felt we were moving from Phase 1 to Phase 2. In retrospect, he was right on the money.

Today in <CITY>Medellín</CITY> we’re working our way through Phase 3. Other emerging expat spots in <COUNTRY>Colombia</COUNTRY>, including <CITY>Cartagena</CITY>, <CITY>Santa Marta</CITY>, <CITY>Bogotá</CITY>, <CITY>Popayán</CITY>, and <CITY>Cali</CITY>, are all at different points along this scale.

To be honest, I’m surprised that <CITY>Medellín</CITY> is catching on so quickly with <COUNTRY>U.S.</COUNTRY> investors and expats. We Americans tend to cast our stereotypes in concrete and treat them as gospel for generations. And, in fact, I still get more questions about drug lords and Pablo Escobar than you’d believe. Remember, Escobar has been dead for 20 years.

The fact that <CITY>Medellín</CITY> has taken on such broad appeal in such a short time is a tribute to those many readers who have been able to discard an outdated stereotype in favor of a terrific lifestyle and a great value.

Were you counting on <COUNTRY>Colombia</COUNTRY> for your next spouse? That’s still no problem, with loads of eligible men and women here.

Has the city been ruined as a sex destination? Maybe, but don’t worry. There’s still plenty of opportunity. In fact, throughout the entire <REGION>Western Hemisphere</REGION>, prostitution is a crime only in the <COUNTRY>United States</COUNTRY>, <COUNTRY>Guyana</COUNTRY>, and <COUNTRY>Surinam</COUNTRY>.

Here’s the real point: If you’re avoiding <COUNTRY>Colombia</COUNTRY> because you think that all it has to offer is women and drugs, then you’re missing out on what I believe to be the best lifestyle choice out there right now…at a time when the market is also offering the greatest opportunity.

Posted by Lee Harrison

Editor’s Note: Lee Harrison will be hosting our upcoming Live and Invest in Colombia Conference taking place in <CITY>Medellin</CITY> in May. We aren’t quite ready to begin taking reservations yet, but you can get your name on the pre-registration list (for advance notice, the deepest discounts, and VIP status) here.