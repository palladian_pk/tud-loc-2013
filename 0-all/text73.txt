Coca-Cola not 'illegally mapping' <COUNTRY role="main">China</COUNTRY> after all, officials say

By Le Li, Producer, NBC News

<CITY>BEIJING</CITY> – When Coca-Cola began using high-tech systems to help its logistics in <UNIT>Yunnan</UNIT> Province, <COUNTRY>China</COUNTRY>, little did it know it would run afoul of that country’s strict “illegal mapping” laws.

Coke? Spying on the Chinese?

Not so fast, say Chinese officials, who have downplayed the charges from <UNIT>Yunnan</UNIT> province that Coca-Cola was collecting sensitive geographical information using handheld GPS devices.

An officer with the agency charged with safeguarding sensitive geographic information denied that the case against Coke is so serious that it involved the powerful Ministry of State Security, as was previously been reported.

“The case is regional, just a branch of Coca Cola,” said an official from the National Administration of Surveying, Mapping and Geoinformation (NASMG), who asked to remain anonymous. 

She added that <COUNTRY>China</COUNTRY> does not hold the soft drink giant in contempt.

“We also have to protect our international companies,” she said. “Coca-Cola is a company that has been an important part of <COUNTRY>China</COUNTRY>.”

A local issue

The controversy erupted after officials in the southern province of <UNIT>Yunnan</UNIT> announced an investigation into charges that Coca-Cola was illegally collecting information on secret areas using handheld GPS devices.  

The case was just one of 21 instances cited in an annual report involving companies allegedly doing illegal surveying in the mountainous southern province of <UNIT>Yunnan</UNIT> which borders <COUNTRY>Laos</COUNTRY>, <COUNTRY>Burma</COUNTRY> and <COUNTRY>Vietnam</COUNTRY>, a tourist destination famous for stunning vistas of jagged snow-capped mountains and rivers.

Gong Yinyong, a spokesman for NASMG, stressed it is still too early to conclude that Coco-Cola has violated Chinese law, and said the investigation was being handled by its <UNIT>Yunnan</UNIT> bureau.

“We appointed our <UNIT>Yunnan</UNIT> bureau to handle the investigation,” Gong said. “The accusation against Coca-Cola <UNIT>Yunnan</UNIT> plant was originally reported to our <UNIT>Yunnan</UNIT> bureau.” 

However, the other officer who spoke anonymously with NBC and works in NASMG’s law enforcement department, confirmed that the agency has been tougher on foreigners and foreign companies in recent years.

Cracking down on foreigners 

<COUNTRY>China</COUNTRY> has been tightening its regulations on the surveying and mapping of its geography, in particular by foreign organizations and individuals. Each year, NASMG picks 10 samples of the most common and important violations and makes them public. Since 2009, at least one case has involved a foreigner every year.  

In 2011, the bureau fined an American man for using handheld GPS illegally in <UNIT>Xinjiang</UNIT> Province and confiscated his equipment. The man had collected and stored 90,000 data points about Chinese geographic locations as he traveled from <CITY>Beijing</CITY> to <UNIT>Xinjiang</UNIT>. He claimed the mapping was part of a plan to open a tour company.

Chinese law prohibits foreigners from “arbitrarily carrying out surveying and mapping” without government permission.

A spokesperson for Coca-Cola sent NBC an official statement, saying that it is cooperating with the investigation to make sure that it coordinates the delivery of its drinks legally.

“Every day, Coca Cola’s trucks deliver beverages to thousands of local retail outlets,” the written statement says. “Some of our local bottling plants in <COUNTRY>China</COUNTRY> have adopted logistics solutions to improve our customer service levels and fuel efficiency.”

The mapping systems used by bottlers are commercially available in <COUNTRY>China</COUNTRY> through authorized local suppliers, the company added.

Military fears

On Thursday a story in the South China Morning Post quoted a mapping expert who suggested <CITY>Beijing</CITY> was sensitive about the use of GPS devices by foreigners because such geographical data could be used by guided missiles to strike key military facilities.  

Professor Weng Jingling, an expert in geographical information systems at <POI>Beijing University</POI>, said the government’s caution would be necessary to protect military facilities. 

“A GPS device can pinpoint the exact coordinates, which a satellite picture cannot provide,” he said.

NBC’s Amy Langfield has contributed to the story.  