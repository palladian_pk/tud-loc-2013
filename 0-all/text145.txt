Jazz@<POI>VIE Bar</POI>, the <CITY role="main">bangkok</CITY> jazz’s destination

<CITY>Bangkok</CITY>--13 Mar--<POI>VIE Hotel Bangkok</POI>

This March, be entertained by top jazz artists including Koh Mr. Saxman, Brian Simpson and Josee Koning.

Every week from Monday to Saturday, 20:30 hours onwards, Jazz@VIE presents passionate live jazz sessions at <POI>VIE Bar</POI>. Chill out in cool, contemporary surroundings while listening to the smooth soulful grooves. Unwind with a superb selection of sumptuous buffet of antipasto, cheese, and wine for only THB 750++ per person*.

On Monday and Tuesday nights, catch Koh Mr Saxman and the talented RA Trio featuring Fangkao on vocals.

On Wednesdays and Thursdays, Koh Mr. Saxman gets expressive with his sax along with the Takeshi Band featuring Lookpad Cholanan and Banyapa ‘Benge’ Sukeenu.

Fridays feature gifted guitarist Neung Jakkawal.

End the week with <COUNTRY>Thailand</COUNTRY>’s national treasure – Fon Monotone and her jazz band.

Check the schedule at http://www.viehotelbangkok.com/restaurant-bars/vie-bar/

Keyboardist, composer and studio musician, Brian Simpson has been the ‘go-to’ man for everyone from Janet Jackson and Teena Marie to George Duke, Stanley Clarke as well as Dave Koz, for whom he has been musical director for 15 years. This chart-topping musical chameleon, who wrote the number one 1 R&B hit “The First Time” for Surface in the 90s, Brian Simpson will be live only at Jazz@VIE on March 9th.

Well known for her interpretation of ‘Musica Popular Brasileira’. Josee Koning was the leading lady of the legendary Brazilian fusion group Batida that recorded three albums, toured internationally and played the North Sea Jazz Festival. In addition, she has released three solo albums to rave reviews and also teaches Brazilian music. On March 29th, Josee Koning will perform with Koh Mr. Saxman and the Takeshi band on. Mark your calendars!

For more information and reservation, please contact +66 (0) 2309 3939, fb@viehotelbangkok.com, and www.facebook.com/viebangkok

*This promotion is available daily from 18:30-20:30 hours only and the price is subject to additional tax and service charge.

<POI>VIE Hotel Bangkok</POI> is a contemporary and creative venue located in the heart of the city, just steps away from the <POI>Ratchathewi BTS</POI> station and <POI>Siam Paragon</POI>, <POI>Platinum</POI>, and <POI>MBK</POI> shopping malls. The chic five-star hotel received its latest recognition of ‘Best Boutique Hotel’ from AsiaRooms.com. <POI>VIE Hotel Bangkok</POI> has also won the ‘Trendy in <CONTINENT>Asia</CONTINENT>’ award in the Tripadvisor Traveler’s Choice Awards, the ‘Best Newcomer in <CITY>Bangkok</CITY>’ award from Wotif.com, and has been nominated as ‘Best Urban Hotel’ by Anywhere Magazine. Its restaurant, <POI>VIE Wine & Grill</POI> recently also received the ‘<COUNTRY>Thailand</COUNTRY>’s Best Restaurant 2010 & 2011’ award from Thailand Tatler Best Restaurant Guide.
