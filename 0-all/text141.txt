5 Must-Do Activities in <COUNTRY role="main">Panama</COUNTRY>

Posted by Fodor's Guest Blogger on March 07, 2013 at 8:57:44 AM EST

Posted in Trip Ideas Tagged: <COUNTRY>Panama</COUNTRY>

By Rebecca Strauss

It's a tiny country (the size of <UNIT>South Carolina</UNIT>) that packs a big punch—<COUNTRY>Panama</COUNTRY>, the umbilical cord between <REGION>Central</REGION> and <CONTINENT>South America</CONTINENT>, boasts a combined <LANDMARK>Caribbean</LANDMARK> and <LANDMARK>Pacific</LANDMARK> coastline of almost 1,800 miles. That's a lot of beckoning beach. Not to mention the lush interior of the country, which offers around 5 million acres of national parks and cooler temperatures when it's blazing hot on the coast. Here are six reasons to go right now.

Eat Like a Local

Skip the ubiquitous chain restaurants in favor of authentic Panamanian fare at <POI>El Trapiche</POI> in <CITY>Panama City</CITY>. Try all the local favorites like ropa vieja, arroz con pollo, empanadas, seafood stew, and all manner of delicious fried starches. Wash it down with a locally brewed Balboa cerveza.

<UNIT>Bocas del Toro</UNIT>

Hankering to get out of the city? Just a one-hour flight from <CITY>Panama City</CITY> is the <UNIT>Bocas del Toro</UNIT> island chain, nestled into the country's far northern <LANDMARK>Caribbean</LANDMARK> coast. The sea is cerulean; the beaches alabaster—pretty much every clichéd adjective used to describe a tropical paradise applies. Though the islands have gotten a lot of press lately as the next undiscovered <LANDMARK>Caribbean</LANDMARK> destination, it remains just enough off the main tourist track to retain much of its authenticity and low-key charm. The six main islands are densely forested, so plan at least one-day hike to a (for now) deserted white-sand beach.

<LANDMARK>Comarca de Guna Yala</LANDMARK>

<UNIT>Bocas del Toro</UNIT> too developed for you? Try the <LANDMARK>Comarca de Guna Yala</LANDMARK>. It's a narrow, 140-mile long strip on the <LANDMARK>Caribbean</LANDMARK> coast, an autonomous region administered by the indigenous Kuna people. The main draw is the pristine <LANDMARK>Archipiélago de San Blás</LANDMARK> (<LANDMARK>Guna Yala</LANDMARK>), nearly 400 islands scattered in the sea like pearls, each of them a castaway's dream. There are a limited number of resorts on the islands, so book both flight and hotel far in advance. Once there, you can snorkel the reefs (no scuba diving allowed unless you bring your own boat and tanks, and get permission in advance), and shop for vibrant, hand-stitched molas, embroidered fabrics made and worn by the Guna people.

The<LANDMARK>Panama Canal</LANDMARK>

It's a must-do for a reason: The<LANDMARK>Panama Canal</LANDMARK> is a spectacular feat of engineering—all the more so for having been completed nearly 100 years ago in 1914. Check it out from the water on either a full or partial transit by boat of the 50-mile route from <CITY>Colón</CITY> to <CITY>Panama City</CITY>. After you've seen the locks from the inside, go back to check out the fantastic museum at <POI>Miraflores</POI>. Make sure to buy the full ticket ($8), which entitles its bearer to a visit of the four-story museum's exhibitions, a short, informative movie, and best of all, to an eye-level view of the gigantic "Panamax" ships transiting the canal from the observation deck outside.

<UNIT>Casco Viejo</UNIT> in <CITY>Panama City</CITY>

Reminiscent of Old San Juan, this elegantly crumbling old quarter of the city dates to the late 1600s. Now a UNESCO Heritage Site, it is undergoing renovations daily, with lovingly restored buildings abutting picturesquely crumbling shells, and hip wine bars and coffee shops sharing real estate with art shops and churches from the 1670s. An afternoon stroll of the cobblestone streets is a must. For an even older city-within-a-city, check out the ruins of <UNIT>Casco Antiguo</UNIT>, on the eastern edge of downtown. Dating to 1519, the city was the first permanent settlement on the <LANDMARK>Pacific Ocean</LANDMARK>, and eventually attained a population of 10,000 people. In 1671, captain Henry Morgan (Captain Morgan of the rum) sacked and burned the city, leaving it the shell of ruins that tourists can visit today. Climb the bell tower of the ruined cathedral for a nice view of the city skyline to the west.