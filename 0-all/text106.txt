<COUNTRY role="main">Democratic Republic of Congo</COUNTRY> profile

A vast country with immense economic resources, the <COUNTRY>Democratic Republic of Congo</COUNTRY> (<COUNTRY>DR Congo</COUNTRY>) has been at the centre of what could be termed <CONTINENT>Africa</CONTINENT>'s world war. This has left it in the grip of a humanitarian crisis. The five-year conflict pitted government forces, supported by <COUNTRY>Angola</COUNTRY>, <COUNTRY>Namibia</COUNTRY> and <COUNTRY>Zimbabwe</COUNTRY>, against rebels backed by <COUNTRY>Uganda</COUNTRY> and <COUNTRY>Rwanda</COUNTRY>.

Despite a peace deal and the formation of a transitional government in 2003, people in the east of the country remain in terror of marauding militias and the army.

The war claimed an estimated three million lives, either as a direct result of fighting or because of disease and malnutrition. It has been called possibly the worst emergency to unfold in <CONTINENT>Africa</CONTINENT> in recent decades.

The war had an economic as well as a political side. Fighting was fuelled by the country's vast mineral wealth, with all sides taking advantage of the anarchy to plunder natural resources.
The history of <COUNTRY>DR Congo</COUNTRY> has been one of civil war and corruption. After independence in 1960, the country immediately faced an army mutiny and an attempt at secession by its mineral-rich province of <UNIT>Katanga</UNIT>.

A year later, its prime minister, Patrice Lumumba, was seized and killed by troops loyal to army chief Joseph Mobutu.

In 1965 Mobutu seized power, later renaming the country <COUNTRY>Zaire</COUNTRY> and himself Mobutu Sese Seko. He turned <COUNTRY>Zaire</COUNTRY> into a springboard for operations against Soviet-backed <COUNTRY>Angola</COUNTRY> and thereby ensured <COUNTRY>US</COUNTRY> backing. But he also made <COUNTRY>Zaire</COUNTRY> synonymous with corruption.

After the Cold War, <COUNTRY>Zaire</COUNTRY> ceased to be of interest to the <COUNTRY>US</COUNTRY>. Thus, when in 1997 neighbouring <COUNTRY>Rwanda</COUNTRY> invaded it to flush out extremist Hutu militias, it gave a boost to the anti-Mobutu rebels, who quickly captured the capital, <CITY>Kinshasa</CITY>, installed Laurent Kabila as president and renamed the country <COUNTRY>DR Congo</COUNTRY>.

Nonetheless, <COUNTRY>DR Congo</COUNTRY>'s troubles continued. A rift between Mr Kabila and his former allies sparked a new rebellion, backed by <COUNTRY>Rwanda</COUNTRY> and <COUNTRY>Uganda</COUNTRY>. <COUNTRY>Angola</COUNTRY>, <COUNTRY>Namibia</COUNTRY> and <COUNTRY>Zimbabwe</COUNTRY> took Kabila's side, turning the country into a vast battleground.
Coup attempts and sporadic violence heralded renewed fighting in the eastern part of the country in 2008. Rwandan Hutu militias clashed with government forces in April, displacing thousands of civilians.

Another militia under rebel General Laurent Nkunda had signed a peace deal with the government in January, but clashes broke out again in August. Gen Nkunda's forces advanced on government bases and the provincial capital <CITY>Goma</CITY> in the autumn, causing civilians and troops to flee while UN peacekeepers tried to hold the line alongside the remaining government forces.

In an attempt to bring the situation under control, the government in January 2009 invited in troops from <COUNTRY>Rwanda</COUNTRY> to help mount a joint operation against the Rwandan rebel Hutu militias active in eastern <COUNTRY>DR Congo</COUNTRY>.

<COUNTRY>Rwanda</COUNTRY> arrested the Hutu militias' main rival, Gen Nkunda, a Congolese Tutsi hitherto seen as its main ally in the area.

In early 2013 the UN secured a regional agreement to end the M23 rebellion in eastern areas, and the group's alleged founder Bosco Ntaganda surrendered to the International Criminal Court to face war-crimes charges.

<COUNTRY>Rwanda</COUNTRY> and <COUNTRY>Uganda</COUNTRY> denied UN accusations that they had supported the M23 group, but the region remains volatile