<CITY role="main">Antwerp</CITY> to charge £230 for foreigners to live there

<CITY>Antwerp</CITY>, <COUNTRY>Belgium</COUNTRY>'s second largest city, is to impose the equivalent of a £230 tax on all foreigners who move to live there.

Under the local law, passed this week and due to come into effect from May 1, citizens from other countries – both EU and non – will have to pay €267 to register with municipal authorities instead of €17 fee charged to Belgians.

Bart De Wever, <CITY>Antwerp</CITY>'s mayor, whose party, the New Flemish Alliance, advocates splitting <COUNTRY>Belgium</COUNTRY> into two states, justified the measure by saying it costs more money to process paperwork for foreigners.

Other supporters of the move however say it is designed to keep out migrants from <REGION>north Africa</REGION>.

"It's nothing to do with keeping out British people," Filip Dewinter, from the Flemish nationalist Vlaams Belang party, told The Daily Telegraph. "It's about all the non-European foreigners who come here without any income. Political refugees, migrants from <REGION>north Africa</REGION> with no money – that's the kind of people we don't want. That's the kind of people who can't afford to pay €246 for everybody they bring over – their children, their parents."

<CITY>Antwerp</CITY>, a city of 1.2 million people, is home to hundreds of thousands of Moroccan, Turkish and Kurdish migrants, most of whom cluster in the same neighbourhoods.

Tensions between Kurds and Turks saw heavy riots in 2011. In the latest incident last September police clashed with Muslim demonstrators angry about an anti-Islamic film in the <COUNTRY>US</COUNTRY>.

A debate in <COUNTRY>Belgium</COUNTRY>'s national parliament last week saw left-wing deputies accuse <CITY>Antwerp</CITY> authorities of bigotry.

"In my eyes, its discrimination, pure and simple," Freya Piryns from the Green party told the assembly.

The Workers' Party of Belgium has lodged an appeal against it in the country's highest court.

The European Commission is checking to see if it complies with EU laws on freedom of movement.

<COUNTRY>Belgium</COUNTRY>'s interior ministry has publicly noted that past EU decisions only forbid higher fees for different foreign nationalities, not blanket fees for all foreigners.