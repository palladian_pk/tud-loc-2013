Norwegian reveals new flights to <COUNTRY>US</COUNTRY> and <CITY>Bangkok</CITY>

Travellers to get new direct flight options out of <CITY>Copenhagen</CITY>, but industry expert said that people better book early if they want a good price

Norwegian's new long-distance routes to <CITY>Ft Lauderdale</CITY> could see prices below 3,000 kroner, round trip (Photo: Scanpix)

Low-cost airline Norwegian has revealed plans to open up long-distance flights to three new destinations from <POI>Copenhagen Airport</POI> within the next year.

From November, the airline will launch direct flights to <CITY>Ft Lauderdale</CITY>, <UNIT>Florida</UNIT> in the <COUNTRY>US</COUNTRY> for 3,000 kroner, and from February 2014, the airline will begin flying direct routes to <CITY>New York</CITY> and <CITY>Bangkok</CITY>.

The news is the latest attempt by Norwegian to challenge the SAS-dominated air superiority in <REGION>Scandinavia</REGION>, something that Norwegian's CEO, Bjørn Kjos, believes will only aid an industry that has seen its share of struggles in recent years.

“Competition is healthy for the industry and that is something that the customers will benefit from,” Kjos told Politiken newspaper.  “The long-distance flight market has been dominated for too long by high prices and little flexibility.”

A quick price check on Norwegian’s website yielded a ticket price to <CITY>Ft Lauderdale</CITY> for as low as 2,768 kroner in December. According to Politiken, tickets to <UNIT>Florida</UNIT> at that time of year are usually around 4,500 kroner and that is with one or more layovers.

Norwegian expects to receive six new Boeing 787 Dreamliner airplanes during the course of 2013, which it plans to use for the new routes. Boeing has had to ground flights of the 787 due to battery problems, but according to the Associated Press, the company expects to have them back in use "within weeks."

“The first planes have already been completed but we still need to complete the test flights,” Kjos said.

Ole Kirchert Christensen, who runs an airline industry news site, said that Norwegian's announced prices were very reasonable, but warned that there was a small hitch.

“It is a good offer, to be sure, but you have to remember that the price is a ‘from’ price so not all the seats in the plane are sold at that price,” Christensen told Politiken. “There may be only 20 out of the 292 seats sold at that price. So people should expect to pay more unless they are fortunate to be among the first to order.”

Some 3.24 million passengers flew from <POI>Copenhagen Airport</POI> using Norwegian last year.