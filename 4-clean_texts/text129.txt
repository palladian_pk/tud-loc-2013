US giants expand here as Ireland reopens for business

Even German software boffins are moving to Ireland as the tech sector booms, say Finbarr Flynn and Cormac Mullen
German tech-nology worker Thomas Maag left Ireland in 2007, just before the Celtic Tiger economy collapsed. Now, he's back.

Maag returned to Dublin last year to manage German customers for eBay's PayPal unit. He is one of an estimated 53,000 people to move to the country in the year through April as Ireland begins to emerge from its worst recession on record.

His experience underscores how Ireland, with the third- fastest projected growth rate in the euro region this year, is restoring competitiveness lost during the boom.

For Maag, falling property prices now make it possible for him to buy a house.

For his employer, declining labour and office costs, coupled with the nation's 12.5 per cent corporate tax rate, make Ireland more attractive.

"Back then, there was no way we could have afforded a house," said Maag, 39, sitting in an office at PayPal's complex in Blanchardstown, Co Dublin. "Dublin is back within our reach."

Ireland's renewed competitiveness makes it a beacon for US companies such as eBay, Google and Facebook, which have expanded their operations in the country over the past two years. For the rest of Europe seeking a path out of the debt crisis, the message is this: take the pain of pay cuts and austerity while resisting pressure to raise company taxes.

"Our competitiveness position has changed dramatically," Minister for Finance Michael Noonan said in a Bloomberg TV interview in London on February 21. "We've cut our cost base dramatically, wages have come down and input costs have come down."

Across crisis-torn Europe, competitiveness is returning. The austerity demanded by policy makers in exchange for aid is lowering wages and office costs, and business is taking advantage. Construction companies in Spain are finding international customers. Germany's Volkswagen AG is adding jobs in Portugal. Greek exports are up 30 per cent in a year.

Ireland's economy crashed in 2008, as a decade-long property bubble collapsed. By January 2012, unemployment had tripled to 15 per cent, the highest since 1994. The State took over five of the six biggest domestic banks, including Allied Irish Banks and Irish Life & Permanent, and the government was forced to seek a €67.5bn international bailout in 2010.

Two years later, exports, at about €170bn, are about 9 per cent higher than in 2007, the height of the Celtic Tiger era. Ireland's ISEQ index, though down 61 per cent since 2007, has risen 37 per cent in the last two years, making it the best performer in Europe over the period.

The nation's borrowing costs have plunged.

The spread, or difference, between yields on Irish five-year government bonds compared with German equivalents has fallen to 2.4 per cent from a high of 15.4 per cent in July 2011, and the Government this month laid the foundation to exit its bailout with its biggest bond sale since the 2010 aid package.

Employment rose on an annual basis in the fourth quarter for the first time since 2008, and unemployment has dropped to 14.1 per cent, the lowest since August 2010. It's been below its peak for a year.

Among those drawn to Ireland is Anne-Charlotte Willot, 27, who last month swapped Lille for Dublin. After graduating with a master's degree in international business in May, she was lured by Approach People, a recruitment firm specialising in placing multilingual workers in jobs outside their home countries. Approach People found her resume online.

"In France, we don't think about coming to Ireland for a job," she said, speaking from the company's offices in Blackrock, Co Dublin.

"Our image of Ireland is because of the crisis, that it was the first to go into recession." In fact, she said, "there are more opportunities here".

Though nowhere near the growth rates of the past, the Irish economy will expand 1.1 per cent this year, the European Commission said on February 22. Only Malta and ЦСЩГТЕКН will grow faster, the commission forecasts.

Ireland is setting standards for other bailed-out countries, International Monetary Fund Managing Director Christine Lagarde told reporters in Dublin on March 8, adding that the nation is seeing the "first fruits of success". "What has been done is huge by any standard," she said.

Irish real unit labour costs fell more than 9.2 per cent between 2008 and 2012, against a 1.6 per cent rise in the EU average, according to Luxembourg-based Eurostat. Between 2005 and 2008, the unit labour costs rose 14 per cent.

Much of the improvement is due to the elimination of relatively unproductive building jobs. Still, workers elsewhere in the economy have taken some pay cuts. In 2009, the year of the deepest slump, 25 per cent of firms surveyed reduced employees' pay, according to the Irish Business and Employers' Confederation. Average weekly earnings fell to €695 in the fourth quarter of last year from €720 in 2008, according to the Central Statistics Office.

It's not just salaries. Prices of everything from offices to shopping centres are down by two-thirds. Prime office rents in Dublin city centre have dropped to €307 per square metre from €673 in the first quarter of 2008, according to commercial property services experts at CBRE.

Home prices have dropped 50 per cent since peaking in 2007, and residential rents are still 25 per cent below their 2007 peak even after rising last year.

That's a boon for the likes of Bruno Banizette-Valentin.

The 42-year-old Frenchman moved to Dublin in 2011 to work for Paddy Power as head of international horse-racing betting operations.

Banizette-Valentin lives in Dublin's gentrified docklands. His two-bedroom apartment overlooks the Liffey. It's close to Grand Canal Square, home to Facebook's European headquarters, the Daniel Libeskind-designed Grand Canal Theatre, bars and bistros.

He pays about €1,800 a month for the apartment, which he shares with his pregnant partner, who works across the square at Google's sprawling European headquarters. He says the area is enlivened by the young Facebook, Google and LinkedIn workers living nearby.

"If you compare Paris, to be able to afford to live in Paris you need to be at least 40. Dublin is a very, very young city and it is probably linked to the fact a lot of people come for their first job," he said, speaking in the Ely wine bar, where he goes to meet some of the French staff. "It's a big melting pot of European people."

In 2011, Google, which employs more than 2,500 people in Ireland, bought the 15-storey Montevetro building close to Grand Canal Square, in a neighbourhood now known as Googletown.

A "robust" investment pipeline is due in "no small part" to falling property prices, the Central Bank said last year.

In 2012, overseas companies backed by the development agency created 12,722 jobs in Ireland, while job losses at such businesses fell to the lowest in a decade. Last week, Yahoo! said it would add 200 workers to its Dublin operation over the next year.

Certainly, pay reductions and freezes and plunging property costs reflect the scale of the crash. About 170,000 jobs have been lost in construction alone since 2007, and the jobless rate is still the fourth-highest in the euro region.

The gains among exporters have yet to translate into much improvement elsewhere in the economy. A net 34,400 people left the country in the 12 months to April, according to the Central Statistics Office.

Retail sales are still falling, and the construction sector continues to contract. About one in four of all mortgages is in arrears or has been restructured, and home prices nationally have resumed their slide.

Taxpayers face another two years of austerity as the Government battles to tame the biggest fiscal deficit in the euro region, at about 7.3 per cent of gross domestic product.

"Austerity alone is not the answer, some stimulus is needed," said Myles Lee, chief executive of CRH, Ireland's biggest company. "We need some of the domestic sectors to pick up to ensure we have a balanced recovery."

Last month, the Government and public sector labour unions agreed on another pay deal. Under proposals yet to be approved by workers, government staff earning more than €65,000 a year face cuts of at least 5.5 per cent. The drop increases to 10 per cent for State employees earning more than €185,000. That's on top of an average 14 per cent cut in pay since 2008.

Among the Government's initiatives to stimulate employment is Connect- Ireland, which involves paying people who make introductions to companies that eventually bring jobs to the country. Eddie Horkan stands to earn €45,000 after suggesting to a contact at US engineering company InterGeo Services that it expand operations in Ireland rather than in Scotland as planned.

A successful tipster is paid a minimum of €1,500 per job, up to a maximum of 100 jobs. InterGeo plans to create about 30 new jobs in Ireland.

"I'm looking for more leads now," Horkan said. "Money isn't the principal motivator. The main motivator is being able to influence the creation of new jobs."

That task is underpinned by Ireland's 12.5 per cent company tax rate, which is "central" to the Government's strategy of targeting overseas investment, according to the Department of Finance.

Two years ago, Taoiseach Enda Kenny vowed to defend the country's tax sovereignty after a "good Gallic spat" over corporate levies with then-French President Nicolas Sarkozy. Ireland's flat corporate tax rate compares with 35 per cent in the US, 33 per cent in France and 24 per cent in the UK, according to the Organisation for Economic Co-operation and Development figures for 2012.

"The Irish Government has obviously come up with a very, very, shall we say, pos-itive tax scheme," said Petter Made, co-founder of SumUp, a mobile-payments company that employs about 35 people in Dublin.

"If you look at the costs, tax versus quality of the workforce and the salaries you have to pay, it balances out quite nicely versus other countries."

While more people are still leaving the country than arriving, some are trickling back. About 21,000 Irish people returned and 32,000 came from abroad in the 12 months to April.

Ten minutes away from Google's headquarters, across Grand Canal Square, Facebook is hiring another 100 people, adding to the 400 it already employs. Increased competitiveness "would be one of many reasons" for expanding in Ireland, said Gareth Lambe, acting head of Facebook's Irish operations. Persuading people to come to Ireland "is quite an easy sell", said Lambe. "It's an aspirational place to move."

About 40 per cent of 1,000 jobs in Ireland announced last year by PayPal probably will be filled by hiring people from outside Ireland, according to Louise Phelan, a vice-president of global operations at the company. While PayPal is working with universities and State agencies to recruit staff locally, for some language skills staff have to be imported, she said.

"These people are spending here," said Phelan. "The knock-on ripple effect of them from housing, property, coffee, and lunch is massive for the greater area and we've seen that truly play out."

Meanwhile, Thomas Maag says he can now afford to look at buying a house in Dublin for his wife and three children. Home prices in the capital have fallen 54 per cent from their peak six years ago.

"If you have a job and the security, and if you can secure a mortgage, you pay half the price or less for the same house," he said. "It would be difficult to bring a similar set-up even in Germany."
