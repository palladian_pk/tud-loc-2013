Chile’s Max Marambio: The executive whose luck ran out
Last July, the International Court of Arbitration in Paris ruled in favor of Chilean businessman Max Marambio, formerly a close friend of Fidel Castro.

Marambio, 63, is suing the Castro regime for $143 million to compensate for the confiscation of Alimentos Río Zaza, a joint-venture food production and marketing entity. He’s also seeking $10 million in “moral damages.”

Marambio said the Cuban government has no legal basis for its case against him and called its lawsuit “political persecution.”

“I was never a saint of Raúl Castro’s devotion, nor of his followers and the people around him,” he told Chile’s Bio Bio Radio.

The International Court ruling  whose jury consisted of lawyers Francisco Orrego Vicuña (Chile), Rodolfo Dávalos (Cuba) and Alexis Mourre (France) puts an end to the commercial alliance between Marambio and the Havana regime, though it doesn’t prevent Cuban authorities from trying him under Cuba’s own penal code.

Marambio, who was once the chief bodyguard of Chilean President Salvador Allende, was accused of bribery, fraud and falsification of bank documents, reported the Santiago newspaper La Tercera. That, in turn, led to the closure of two of his plants in 2010 and the seizure of $23 million of the venture’s assets.

At the time, Marambio argued that he had to shut the company because of financial measures imposed by Raúl Castro, such as the freezing of payments to foreign firms.

For the leftist Chilean businessman, who couldn’t be reached to comment on this story, Cuba had become a sort of Treasure Island.

Formerly chief of the so-called Grupo de Amigos del Presidente [Friends of the President], Marambio fled to Cuba following the 1973 military coup that overthrew Allende.

He soon developed a close relationship with Fidel, rising to the rank of lieutenant-colonel of special troops with Cuba’s Revolutionary Armed Forces.

Marambio eventually received the most important award of the Cuban revolution.  He also made a fortune in the 1990s from his stake in Río Zaza, a near-monopoly joint venture that sells fruit juice and dairy products.

At one point, Marambio controlled more than 30 companies in Cuba’s industrial, tourism and real-estate sectors, along with commercial airlines in Cuba, Chile, Spain, Mexico and Ecuador through a holding company, International Network Group (ING), which had revenues in excess of $100 million a year.

In May 2011, the Communist Party mouthpiece Granma announced that a Cuban court had sentenced Marambio to 20 years in absentia. In addition, Cuba’s 75-year-old former food minister, Alejandro Roca, received a 15-year jail sentence for bribery.

The following month, Marambio’s brother, Marcel ING’s vice-president and the CEO of Sol & Son agency was also sentenced in absentia to 15 years, along with 15 board members of the state-run airline, Cubana de Aviación, for “crimes of corruption with international ramifications.”

Both brothers are former members of the leftist Movimiento de Izquierda Revolucionaria (MIR), and live in Chile. When they did not respond to Cuba’s summons to appear for trial, the Havana government petitioned for their arrest.

Juan Pablo Hermosilla, Max Marambio’s lawyer, told Chile’s Radio Cooperativa that the rigorous sentence against his client is the result of a “political struggle” between Fidel and Raúl. He added that “according to Chilean law and international conventions, it is forbidden” for such a trial to take place in absentia.

The Marambio case was made public one day after the untimely death of Roberto Baudrand, general manager of Río Zaza. Baudrad’s death is said to have occurred under strange circumstances. Cuban authorities claim he died from respiratory complications combined with drugs and alcohol found in his system.

Many people wonder how Marambio made so much money, given that while he’s certainly not the wealthiest man in Chile, he’s the only one who earned his fortune in the heart of Latin American socialism.

A flying enthusiast, Marambio owns a Bell 206 helicopter. At one point, he also owned a huge house in Havana, real estate in Madrid and an estancia in Pirque, near Santiago. He has been married three times; his current wife is Esperanza Cueto, a lawyer and a board member of LanChile.

An April 2010 Communist Party report stated that Río Zaza had applied illegal surcharges and committed fraud. The report also stated that Marambio and Alejandro Roca had caused “considerable damage to the nation's economy” and “impaired the ethical behavior of various officials and subordinate workers.”

This would have provided probable cause for the detention of various collaborators of Marambio. On Mar. 8, 2010, Granma reported the firing of the president of Cuba’s Civil Aeronautics Institute, Rogelio Acevedo González, who was implicated in an operation involving embezzlement.

Later on, Acevedo’s wife, Ofelia Liptak, marketing director of Río Zaza, was also arrested along with Lucy Leal, an accountant of Marambio’s companies in Cuba. Both arrests were based on suspicion of embezzlement and the illegal diversion of money to foreign countries.
