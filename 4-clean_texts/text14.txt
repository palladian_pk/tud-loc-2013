By Phil Marty, Special to Tribune Newspapers
6:09 p.m. CST, December 2, 2012

Won't be long till the holidays will be over and then there's that interminable spell of cold, gray days. But don't despair. There are days (and nights) ahead promising snowmobile racing and film festivals and hikes and hot-air balloons — and even appreciation for bald eagles.

Here's a sampling of events in the Midwest through the winter:

Illinois

Through Dec. 31 — Way of Lights, Belleville. The National Shrine of Our Lady of the Snows offers this light display that tells the birth of Jesus, from the journey to Bethlehem to the Nativity. Free, 618-397-6700, wayoflights.org. Miles from Chicago: 295

Jan. 13 — Volo Bog's Winterfest, Ingleside. Indoors there will be live music and storytelling along with various crafts. Outdoors there will be hikes around the bog and snow sculpting and cross-country skiing, if the weather cooperates. Free, 815-344-1294, tinyurl.com/cro5kto. Miles from Chicago: 55

Indiana

Jan. 18-20 — Winter Wellness Weekend — Warm UP from Within, Nashville. Brown County State Park hosts this weekend of yoga, hiking, a trail run and indoor fitness-related activities. Fee applies, 812-988-9642, tinyurl.com/asu6jmq. Miles from Chicago: 235

Feb. 10 — 13th annual Old Post Bluegrass Jam, Vincennes. Bluegrass musicians are invited to bring their acoustic instruments for a day of playing. Free, 800-886-6443, tinyurl.com/adnab37. Miles from Chicago: 255

Iowa

Jan. 5-6 — Iowa Diecast Toy Show, Des Moines. Vendors from around the Midwest offer new and older die-cast toys, including farm toys, cars, semis, pedal tractors and more. Fee varies, 515-266-8697, tinyurl.com/dyubmck. Miles from Chicago: 335

Jan. 19-20 — Bald Eagle Appreciation Days, Keokuk. See bald eagles flying, fishing and roosting along the Mississippi River at Lock and Dam No. 19. Indoors there will be live raptors, seminars and conservation-related exhibits. Free, 800-383-1219, tinyurl.com/ycn5lfp. Miles from Chicago: 270

Michigan

Jan. 18-20 — Caro 150 Winter Fest and Snowmobile Races. Professional snowmobile racers tear around the track in a variety of events Saturday, followed by a 150-mile contest Sunday. There's also music, ice-carving demonstrations and lots more. Fee varies, 989-673-7424, http://www.carowinterfest.com. Miles from Chicago: 325

Jan. 18-20, 25-27 — Tip-Up Town USA, Houghton Lake. This is the 63rd year for this event, billed as the largest winter festival in Michigan. There'll be snowmobile racing, hockey, a parade, beer tent with music and more. Fee applies, 989-366-5644, tinyurl.com/bstsrms. Miles from Chicago: 320

Minnesota

Jan. 18-20 — U.S. Pond Hockey Championships, Minneapolis. Teams from across the country come to Lake Nokomis for outdoor hockey competition. Free for spectators, uspondhockey.com. Miles from Chicago: 410

Jan. 24-26 — Red Bull Crashed Ice, St. Paul. See the world's fastest ice-cross racers on a frozen downhill course full of twists, turns, canals and chutes. Prelims run Thursday and Friday, with the finals Saturday. Free, tinyurl.com/aq8glcg. Miles from Chicago: 400

Missouri

Jan. 6-Feb. 12 — Soulard Mardi Gras, St. Louis. Twelfth Night, with a musical procession and fireworks, kicks off the celebration, and the action continues for more than a month with a winter carnival, Cajun cook-off, parades, music and more. Fee for some events, 314-771-5110, tinyurl.com/ayh7nsn. Miles from Chicago: 300

Feb. 28-March 3 — True/False Film Festival, Columbia. What started as a tiny festival has grown into a much bigger event. Lots of film screenings, talks, seminars and more. Fees vary, 573-442-8783, truefalse.org. Miles from Chicago: 385

Ohio

Jan. 19 — Annual Winter Hike at Hocking Hills State Park, Logan. Here's a chance to tackle a six-mile hike in the beautiful Hocking Hills area. See frozen waterfalls and cave walls covered in "ice beards." Free, 740-385-6841, tinyurl.com/cyr46k7. Miles from Chicago: 405

Feb. 28-March 3 — Arnold Sports Festival, Columbus. More than 15,000 athletes compete in 37 events and 11 Olympic sports, and a three-day expo includes more than 650 exhibitors offering fitness apparel, equipment, training tools and more. Fees vary, 614-431-2600, arnoldsportsfestival.com. Miles from Chicago: 355

Wisconsin

Feb. 1-3 — Hot Air Affair, Hudson. Cold air meets hot air in this 24th annual rally featuring dozens of colorful hot-air balloons. There's also a kite show, live entertainment, ice carving and more. Free, 800-657-6775, hudsonhotairaffair.com. Miles from Chicago: 380

Feb. 22-24 — Madison Fishing Expo. They call this the largest fishing expo in the Midwest. You'll find lots of tackle, boats, seminars and a trout pond. Fee applies, 608-245-1040, madfishexpo.com. Miles from Chicago: 150
