Czech Republic: readers' travel tips

Prague is a justly popular destination, but our readers also have walking, cycling and eating tips in smaller Czech cities, such as Brno and Cesky Krumlov
Winning tip: Adrspach, Teplice Rocks

An area of 17 square miles in north-eastern Bohemia, 100 miles from Prague, this national park is made up of sandstone that has been eroded over the years into fantastic shaped rock formations. A marked pathway, taking about three hours to walk, takes you past and through the formations, with names such as the Caterpillar, Butcher's Axe, the Dog and Boar, and the Sphinx and the Golem. You should also try climbing the ladders to the ruins of Strmen Castle, now a viewing platform. adrspach.com

manxlongtail

Prague

Alfons Mucha's The Slav Epic paintings

Czech painter Alfons Mucha created this cycle of 20 huge canvases in the art nouveau style. They are on show at the Veletržní Palace, part of the national gallery, and are monumental. See it while you can, though, as it is destined for a permanent home, when Prague can agree where and how to fund it.

Veletržní Palace, Dukelských hrdinu 530/47, ngprague.cz; open Tue-Sun 10am-6pm, adults 180 Czech koruna (£6.20)

davidwitt

Art deco Imperial hotel bar and cafe

You might think the art deco Imperial hotel is too posh for you and your kind, but think again. I visited after a hard day's work wearing shabby clothes and the staff didn't bat an eyelid. On the ground floor is a beautiful cafe bar decorated with art nouveau mosaics. Treat yourself to a cocktail and enjoy the splendour of your surroundings.

Na Poríci 15, +420 246 011 600, hotel-imperial.cz; mains at Café Imperial from £8.60

orbki32

Beer gardens

Prague has a surfeit of beer gardens in parks around the city, where small shacks sell beer and wine to a laid-back crowd. One of the most romantic is in Letenské park, with fantastic views over the city. There's often someone strumming a guitar – and there's no better place to enjoy a summer afternoon. There is also a restaurant in an impressive neo-Renaissance chateau in the park should you feel like treating yourself.

Restaurant Hanavský Pavilion, Letenské sady 173, +420 233 323 641, hanavskypavilon.cz; open daily 11-midnight, mains from €14

hariad

Obecni Dum (Municipal House)

The cultural tour of this Prague landmark (and concert venue) takes you around areas you would only see if you went to a number of concerts – there's some stunning art and architecture plus an interesting history.

námestí Republiky 1090/5, +420 222 002 780, obecnidum.cz

goose64

Petrin Tower

Buy a one-day travel pass and rumble along on a number 22 tram from the National Theatre across the Voltava river, then let the funicular railway take the strain up Petrin Hill. The Petrin Observation Tower is an Eiffel lookalike and sways slightly in the wind. You can go up by lift or climb the 299 wooden stairs on the outside to the viewing platform. Stroll back to the city through gardens and apple orchards. Two hours, all in, for a fiver.

Petrínské sady, petrinska-rozhledna.cz

julianwilde

Zlatý Klas restaurant

This is an excellent, authentic Czech bar-restaurant. It is out of the city centre, and west of the river but not so very far away. You can eat your fill for a very modest outlay.

Plzenská 9,Praha 5, +420 251 562 539, zlatyklas.cz; traditional Czech dishes from £6

markscott

Moravia and east

Brno

People say: "Oh, I've been to Prague," but Brno, the second city, is a terrific alternative. Cheaper, for one thing, and without McDonald's or Starbucks everywhere. Countless bars serve great food and beer, the views from Spilberk (the old fortress, spilberk.cz) and the cathedral are excellent, and the city can be easily negotiated on foot, unless you fancy the tram system, which extends over most of it.
sameagainplease

Litomysl castle and theatre

The castle in Litomysl is Unesco world-heritage listed and its baroque 150-seat theatre is one of only five in the world. The town is full of Renaissance and baroque buildings and the old brewery is the birthplace of the composer Smetana. The monastery gardens, with individual sculptures and subtle background music, are another attraction – as is the good, cheap beer and food. Hotel Aplaus (Šantovo námestí 181, +420 4616 14900, hotelaplaus.cz) has doubles from £80.

jadeyr

Kromeriz and Mikulov

Less than 100 miles apart, these South Moravian towns are linked by good cycling routes where you can enjoy vineyards around the Morava river, the smell of flowering lime trees, baroque buildings, palaces and gardens, and market square cafes. In Kromeriz the highlight is the Radnicni restaurant +420 608 117 226, (rs-kromeriz.cz), serving a tasting menu with local wines. In Mikulov, Hotel Templ (+420 519 323095, templ.cz, doubles £57 B&B) provides comfortable rooms and excellent food, and was a good base for a day's cycling exploring the former Liechtenstein palaces of Valtice and Lednice, and the former Jewish area of Mikulov itself.

carolinecyclist

South

Cesky Krumlov

After a few busy days in Prague, Cesky Krumlov is a refreshing getaway three to five hours south of the capital by bus. You follow the Vltava river 180km south from Prague towards the German border to this Unesco world heritage site set in a tight loop of the Vltava. It is dominated by an imposing castle built on sheer rock which gives stunning views over the medieval houses and narrow streets. The castle has a bear pit (complete with bears) and an historic theatre where original costumes are still worn for performances. Well-informed, multilingual town guides are very helpful and not expensive to hire for two or three hours. Families will love canoeing on the winding river through gorges and the town itself. And there are beautiful and undemanding walks in the gentle surrounding countryside and forest.

ckrumlov.info

carolinesw13

Nationwide

Walking

The Czech Republic has an amazing network of well-marked colour-coded footpaths. Outside almost any railway station (and there are often railway stations in the smallest of villages) you'll find a sign indicating a variety of walking routes, with distances and likely times. Good walking maps (I found four different publishers) are widely available for much of the country.

martinlunnon
