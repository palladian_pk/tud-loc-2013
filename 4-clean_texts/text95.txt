Belgium Beyond Brussels

Posted by Jill Fergus on October 17, 2012 at 11:06:26 AM EDT

Posted in Trip Ideas Tagged: Belgium, Brussels

If you've been to Belgium, chances are you've spent time in Brussels but there are plenty of other Belgian cities worth adding to your itinerary. Places like Bruges and Antwerp in the Flemish-speaking northern region (Flanders) and Liege and Namur in the French-speaking southern region (Wallonia). And since many cities are just an hour from Brussels by train, you can see a lot in a short amount of time. Here are five our favorite places in Belgium... beyond Brussels.

Bruges

What to Do: Bruges, with its romantic canals and gabled buildings, is one of Europe's most charming cities. Start at 13th century Market Square dominated by the medieval Belfry or bell tower (if you're feeling adventurous climb the 366 steps for great views), then hop aboard a small boat for a thirty-minute canal tour. At De Halve Maan, a family-run brewery dating from 1564, take a tour then sample the Straffe Hendrik ale and with more than 50 chocolate shops, you won't have trouble satisfying your sweet tooth—though pralines are recommended.

Where to Eat: For a casual lunch, eat at one of the cafes lining Market Square. People-watch as you savor local treats like moules frites and a mug of Orval beer. An upscale dinner option is Zeno housed in a restored mansion where chef Reinout Reniere puts a modern spin on classic Belgian dishes.

Where to Stay: The 93-room Kempinski Hotel Dukes' Palace in a former ducal residence features frescoes, stained glass windows, and a manicured garden. Have a drink in the Atelier Bar lined with local artwork.

Liege

What to Do: Architecture buffs are making a pilgrimage to Liege, Belgium's third largest city, to check out Santiago Calatrava's redesigned Liege-Guillemins, the high-speed train station with an impressive soaring curved steel-and-glass roof. Browse among the decorative and religious art in the Grand Curtius Museum in a restored mansion on the Meuse River. And in nearby Aubel, visit Val Dieu Brewery on the grounds of the Val Dieu abbey, built in 1216 by Cistercian monks. After an abbey and brewery tour, sip its three main beers: blonde, brown, and triple.

Where to Eat: For a traditional meal, head to Chez Philippe (Rue Haute-Sauveniere 39, Liege 4000) for boulets a la liegoise (Liege meatballs) served with a side of frites. Though guests can arrive via helicopter at Restaurant Heliport most take a taxi to reach this upscale waterfront spot with a French-seafood menu.

Where to Stay: Another architectural addition to the town is the new Crowne Plaza Hotel, in two former nobleman's 16th century mansions with modern rooms, the Osmose Spa and Le Cave bar in the vault.

Ghent

What to Do: Like Bruges, Ghent also has peaceful canals flowing through its city center alongside towering medieval buildings. One of the most visited is the St. Bavo Cathedral home of the world-famous 24-panel Ghent Altarpiece, by the Van Eyck brothers, located in a side chapel. Another must-see attraction is the imposing Gravensteen Castle with medieval weapons exhibits and panoramic views from the parapets. After a day of touring, stop by T'Dreupelkot, a postage-stamp sized wood-paneled bar for a glass of genever, a delicious gin-like liqueur.

Where to Eat: One of the young Flanders chefs doing innovative things in the kitchen is Olly Ceulenaere, whose industrial-chic Volta in a former power plant serves dishes like local Moerbeke lamb with carrots, grey shrimp with beetroot, and cream of smoked mackerel.

Where to Stay: In Patershol, Ghent's oldest neighborhood, is the Hotel Harmony, a 24-room inn with an interior garden in two restored buildings. Attractions like the Gravensteen castle are a five minute walk.

Namur

What to Do: For a bird's-eye view of the area, climb the Namur citadel, dating from Roman times. In town, flanked by the Sambre and Meuse rivers, shop for antiques and crafts and stop along the way for coffee and a chocolate croissant in small patisseries. Comptoir de l'Oliviat is a beloved food emporium that sells local products like Wepion strawberry jam and Upignac foie gras. A short drive from the city center you'll find the water gardens of Chateau d'Annevoie, a large complex of formal flower gardens, ornamental ponds, fountains, and cascades.

Where to Eat: Les Temps des Cerises serves traditional fare like wild boar from the Ardennes while in the nearby village of Lavaux-Sainte-Anne is Michelin-starred Lemonnier with seasonal cuisine (guinea fowl cooked in hay) created by father-and-son team, Eric and Tristan Martin.

Where to Stay: Comprised of a series restored 17th century houses built between 1670 and 1716, Les Tanneurs de Namur has 32 individually designed rooms and the elegant L'Espieglerie restaurant.

Antwerp

What to Do: This city of high fashion attracts shoppers from all over. There's the pedestrian-only Meir where you'll find the Stadsfeestzaal, a 40-boutiqe upscale shopping hall with marble and mosaics, as well as the Fashion District with names like Dries van Noten and Ann Demeulemeester. There's also the jewelry shops in the Diamond District—80% of the world's diamonds pass through here. Fans of artist Peter Paul Rubens can visit Rubenshuis, his former mansion and atelier as well as St. Paul's Church, which has numerous paintings on display.

Where to Eat: You can't leave Belgium without trying one of its famous waffles, topped with strawberries and whipped cream and the Van Hecke Waffle House, open since 1905, is rumored to have the best. Lux, a stylish bar/restaurant in the hip Docklands is winning raves for its fusion cuisine.

Where to Stay: At the Hotel Julien the 21 rooms come with contemporary furniture, white duvet-topped platform beds and flatscreens. Have a coffee or glass of wine on the rooftop terrace with views of Antwerp cathedral.

Photo Credits:Bruges Kempinski Hotel Dukes' Palace: Kempinski; Liege Guillemins Train Station: Dutchy / iStockphoto.com; Ghent: Chanclos / Shutterstock.com; Namur: Mcsdwarken | Dreamstime.com; Antwerp: Arpad Benedek/iStockphoto.com

More by Jill Fergus, Fodor's Contributor
