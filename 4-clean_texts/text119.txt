Planning Your Easter Holidays To Spain

Guest Post

So you’ve well and truly banished the wintertime blues. The sun has come out and you have a new-found spring in your step. You be in such a good mood that you decide to treat the whole family to an Easter holiday in Spain. Now all that you have to do is schedule your travel itinerary. Thankfully the team Directline-Holidays have produced this helpful guide to the noteworthy festivals and activities that you may enjoy this Easter.

Semana Santa

The Easter festival dates back to the 16th century in Spain. It was at this time that the Spanish authorities decided to arrange a procession, during which the story of the passion of the Christ would be retold. Each year the Spanish people take to the streets of Madrid, Toledo and other cities in order to carry on the tradition. You are encouraged to come and watch as large floats bearing models of Christ and the Virgin Mary are carried about by members of the Christian faithful.

Fiesta Del Bollo 

Many people think of Easter as the perfect time to indulge in delicious sweet foods. In the UK we have hot cross buns, simnel cake and chocolate eggs. In the Spanish village of Aviles they prefer to eat special Del Bollo pastries. You can also join the crowds for a great picnic on one of the streets or squares in the picturesque region of Asturias. Make sure that you stick around for the evening fireworks and traditional Mediterranean music.

Semana de la Musica Religiosa

Those of you who are planning on staying in Spain for a couple of weeks will have the opportunity to catch a number of religiously inspired musical performances at the local churches and cathedrals. You are invited to stay in the city of Cuenca for the Camino del Calvario procession, when drums and trumpets are played in honour of the lord. It will also be possible to catch performances by some of the best European choirs and orchestras.

Bullfighting In Seville

The people of Seville are very passionate about bullfighting. They gather regularly in the Plaza de Toros de la Maestranza, to watch highly skilled matadors go head to head against the crazed beasts. If you’re keen to see this major Spanish event then it would be worth booking tickets in advance. Alternatively you might want to head across to the Estadio Olimpico de Sevilla to watch the local football team compete for the La Liga title.

(Though it’s a little but gruesome for some tastes.)

Other Possibilities

It is worth bearing in mind that the vast majority of Spanish attractions remain open throughout the year. You could take a guided tour round the awe inspiring Segrada Familia or the Mezquita of Cordoba this Easter. You may also want to spend some time on the picturesque Spanish beaches. There’s so much to see and do in this part of the world that you’ll be wanting to book more than one break with Directline-Holidays.
