Vilnius to Tbilisi in unreasonably cheap cars: how it all started

Posted on 2013-01-18

Perhaps the cheap and barely drinkable Egyptian rum is to blame. As we were happily drinking that filth for yet another Egyptian holiday evening, once again a rather old idea to travel somewhere exotic in old Mercedeses bubbled up. Mantas had already proposed a trip somewhere to the hinterlands of Central Asia a few years back (no doubt, inspired by the Lithuanian travelers who did a similar trip and made a TV series about it), but at that time we chickened out: we were young, we were inexperienced and the financial crisis was just unfolding and showing its claws, so money was tight. Things are very different now: we are still young and we are still inexperienced, but at least the crisis is somewhat behind us, so we can return to our dreams about being real men, taming the Eastern European wilderness, wrestling with bears and feeling super cool.

The next day we ruled out Central Asia as our destination, since the sights of scary looking unshaven guys with AK-74s manning the roadblocks every 20 kilometers in Egypt brought us to our senses (also, smaller amounts of rum were consumed in the morning). We have abandoned Central Asia as a destination as we thought that Uzbekistan or Iran is a bit too much to handle on a first serious trip outside the comfortable and cozy European Union civility. Georgia, however, would be great. It is an exotic place right on the edge of Europe and Asia, cheap enough to fly back from and full of very friendly Georgians, who regard Lithuanians as their best friends (trust me, I have nowhere felt more welcome). The route to Tbilisi would also offer a wide range of opportunities to sample all the different countries in the Balkans: see the nature, taste the slivovitzas, eat the tasty Ćevapčići and practice my few Serbo-Croatian phrases (I can almost read the financial statements in Croatian, but the phrases that I can use in every day situations are limited to “tvoja majka je lepa” and “kiša pada, gde je tvoj kišobran?” which roughly translate as “I like your mum” and “give me your umbrella! NOW!“. On second thought, I should probably not use any of those phrases at all.)

It is not that we haven’t been to all those countries on our itinerary at some point in our lives. I have visited them all except Albania and Macedonia and I have missed those two just because they lacked reasonably functional stock markets. Most of us have visited Georgia (perhaps I repeat myself, but it’s the only place in the world where Lithuanians can feel like demigods), and a few have done business in Ukraine or even Belarus. However, I have a sneaky suspicion that seeing a place like Sarajevo from the window of a five star hotel during the investors’ conference while being treated as royalty because you have a business card that says “fund manager who has no clue, but eager to invest” is slightly different from traveling the country in an old nearly broken Mercedes and meeting the real people. One has to wonder if our mad Excel and IT skills are really applicable in the real world when one has to jump-start a car, change a flat tire or convince a friendly local to show us the way to the closest bar using only sign language.

The rules that we have just made up are simple:

Dates: two weeks starting on April 19th

We buy 2 cars (probably Mercedeses), manufactured before 1990 old enough, for about $1000 (with extra repairs and the expenses covering preparation for the trip) in Lithuania

We sell them in Tbilisi, Georgia (hopefully — anyone wants to take them off us at an attractive price?)

A tightly-knit group of friends, consisting of men only

The proposed route on Google Maps (may be revised)

Stay cheap: couchsurfing, hostels and camping sites

This is the blog where we intend to keep our trip updates. Tbilisi, here we come!
