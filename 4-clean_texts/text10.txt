Gunmen kill senior women’s activist in Afghanistan

KABUL — A senior advocate for women in Afghanistan was shot dead by unknown gunmen on Monday, officials said, the latest assassination against women’s rights activists in the country.

Two assailants riding a motorbike gunned down Najia Seddiqi as she was heading to her office in eastern Laghman province, said Helai Nekzad, the chief of information at the Women’s Affairs Ministry in Kabul.

Seddiqi was head of the Women Affairs Department for Laghman province. Her predecessor in that post was killed four months ago, when explosives hidden in her car were detonated.

“We have launched an investigation to find out whether Najia Seddiqi’s killing was politically motivated,” Nekzad said.

President Hamid Karzai described the assassination as a “terroristic” one, a term he often uses to describe attacks either by Taliban-led insurgents or al-Qaeda militants. No group or individual has made any claim of responsibility.

The Taliban has yet to comment on the shooting, which comes a week after a teenage girl, volunteering in an anti-polio drive, was fatally shot northeast of Kabul. Officials have said they do not believe the girl’s death was politically motivated.

The Taliban has targeted senior female officials in the past for working in the U.S.-backed Afghan government. But other attacks on women have been linked to family members of the victims, who resent their female relatives working in the government or promoting the cause of women in Afghanistan’s deeply traditional and male-dominated society.

Elsewhere in Afghanistan on Monday, the police chief for southwestern Nimruz province, Musal Rassouli, was killed in a car bombing. Two of his bodyguards were wounded in the explosion, officials said. The Taliban claimed responsibility.

Monday’s assassinations followed an attack Thursday in which a Taliban suicide bomber targeted Afghanistan’s top intelligence officer, Asadullah Khalid, at his private guest house in Kabul. Khalid was wounded in the attack and is being treated at a U.S. military hospital.

In a separate development, the Taliban said it will send two of its political delegates to a meeting in Paris on Dec. 17, when Afghan and non-Afghan politicians and officials are expected to discuss Afghanistan’s future.

In a statement, the Taliban said its delegates would share their organization’s views on how the long U.S. military presence in Afghanistan can be ended. The statement denied media reports that Taliban officials would participate in peace talks at the meeting.
