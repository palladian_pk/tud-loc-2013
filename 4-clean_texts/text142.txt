The Saudi princess, the fake sheikh and a plot to silence her

She is the granddaughter of Saudi Arabia’s first king and the youngest daughter of its second ruler.

Despite her royal heritage, the Saudi princess has long been a moderate and a human rights campaigner in a kingdom not known for its liberal outlook.

Now, Her Royal Highness Princess Basmah bint Saud bin Abdulaziz al Saud – to give her her full title – faces her most arduous battle.

The princess, who lives in west London, has been blackmailed over a revealing video which she insists was stolen from her laptop by computer hackers.

The video appears to Western eyes innocuous enough, but to the Saudi establishment, which prefers neither to hear nor to see its women, it is deeply embarrassing.

The footage shows Princess Basmah blowing a kiss to the camera, smoking a cigarette while her long brown hair is uncovered, against Saudi tradition.

Her blackmailer has demanded more than £300,000 not to publish the contents of conversations the princess was tricked into having, as well as videos and photographs allegedly stolen from her computer.

But rather than pay up, Princess Basmah, 48, a mother of five children, has decided to go public to pre-empt the blackmailer.

“In Saudi Arabia it is a scandal for a woman to smoke and not have anything on her head,” Princess Basmah told The Sunday Telegraph.

“It is like seeing Princess Kate [the Duchess of Cambridge] with no bra. It has the same effect. And since I am a symbol – fighting for humanitarian causes and fighting corruption, and somebody with a moderate Islamic message, that would ruin everything for me with my public in Saudi Arabia.”

Princess Basmah is in some ways an unlikely member of the Saudi royal family. She was the youngest daughter of King Saud, who ruled the kingdom from 1953 to 1964 until he was forced into exile in Europe after being accused of corruption. Princess Basmah was the youngest of his 115 children and saw her father only twice before he died when she was only five.

The princess established herself in Saudi Arabia as a writer, businesswoman and charity worker, but moved to London two years ago.

She insists she has not fallen out with Saudi’s ruling elite – and now lives in the west London suburb of Acton in a £1.5 million house.

The plot began while Princess Basmah was on holiday at Christmas at The Gleneagles Hotel in Scotland. Logging on to her Facebook account, she was contacted by a friend, whom she refuses to name, but describes as a 30-something sheikh from the United Arab Emirates. In fact, the sheikh’s account had been hacked by the blackmailer. The plot to undermine her had begun.

“I won’t say the name [of the sheikh]; I don’t want to hurt him,” said Princess Basmah, “But it was through his Facebook account that I was hacked. It was one of the prominent sheikhs.

“He sent me a message saying how fascinated he is by my work, how he admired my writings and how I was a hero of the Arab world.”

Flattered by the praise, the princess and the man she mistook for a sheikh continued their dialogue for a further three days before having a long video conversation via the internet, using Skype.

“We talked around December 27th,” said Princess Basmah, “He was a young man. He started getting emotional, saying he would be coming to see me and that I had to see him. He said he was very attracted to me and I started to become suspicious.”

The princess believes the man, who resembled the sheikh, was trying to entrap her into making injudicious and overtly sexual comments.

She confronted the man whom she now suspected of fraud and he revealed his true motives.

“He told me: 'I am not so-and-so [the sheikh], I am a blackmailer’,” recalled the princess. “I was speechless. I thought, why would somebody blackmail me?

"There are lots of people who want to get me in trouble but why blackmail me? There is nothing they can expose me on. I am a very public person. I have nothing to hide.

“I felt like I was physically beaten up. I felt bruised psychologically and mentally.”

The blackmailer demanded $500,000 (£320,000), asking money to be transferred to accounts in Egypt but the princess went public first.

On her website, she posted: “I have been threatened and blackmailed by a large organisation which has wielded its power to try and destroy my reputation.

"I have recorded these attempts and have evidence which I will publish via social media exposing the attempts to quiet my voice.”

The blackmailer then published a 40-second video of the princess on the internet site YouTube, showing her smoking a cigarette and blowing a kiss. Her head is uncovered.

The princess insists this is not footage taken from their Skype conversation. She says the video was stolen from her laptop and has no sound but shows her talking to her son by Skype.

To add to the intrigue, a few days after the blackmail plot the princess’s Range Rover was stolen from outside her house.

She is now convinced she is being watched and has brought in a computer expert to check for bugging devices on her computer and in her home and other cars.

Princess Basmah is certain the blackmailer is part of a network of activists and dissidents trying to undermine the royal family and her relationship with the kingdom’s rulers.

She is convinced the blackmailer has connections to an anonymous Twitter user called Mujtahidd, who has almost one million followers and has published intimate details of other members of the Saudi ruling family.

“The Twitter site is very well known in Saudi Arabia and the Middle East. Everybody gets information from it. Mujtahidd really tells you what is happening inside the country. The blackmailer told me he was one of them [in Mujtahidd’s network] and he told me information to make me believe him. His is a big, huge network.”
