Communism making comeback in Czech Republic

By Jason Hovet and Robert Muller, Reuters

PRAGUE -- Before the fall of communism, Vaclav Sloup trained the soldiers who caught thousands trying to flee across Czechoslovakia's fortified border to West Germany.

Today, he helps power a Czech Communist Party that has surged to second place in polls, tapping anger over poverty and graft.

When communism collapsed in 1989, the once-dominant party was slow to produce the same kind of reformist wing as emerged in other Eastern European countries. While statues of Lenin have vanished and the party insists it has reformed, its lawmakers still greet one another with "comrade" and maintain hard-line foreign policy views such as leaving NATO.

The Communist Party of Bohemia and Moravia remains a pariah to many, but it is a pariah commanding 20 percent of the vote.

Sloup's rise to become education councilor in the northern region of Karlovy Vary has aroused protests by students and former dissidents that demonstrate the strength of emotion, but the 63-year-old refuses to quit because of his past.

"I carried out my duties when Europe was divided by an Iron Curtain, and it was in accordance with laws of the time," he said. "I have nothing to be ashamed of." 

However, two Communist councilors, one a former border guard and secret police informant, resigned after demonstrations against the party in Bohemia.

"The Communist Party is anti-democratic," said David Pithart, a 47-year-old environmental consultant, whose father Petr was a leading dissident and served as Czech prime minister in 1990.

"The party doesn't know how to govern any other way," he said while at a protest in Ceske Budejovice in December. 

Second most popular party

The communists have jumped past ruling conservative parties to second place in polls and would be a prospective partner for the poll-leading, center-left Social Democrat Party after a 2014 general election.

Social Democrat leader Bohuslav Sobotka now talks openly about prospects of forming a minority government backed by communists. He wants to avoid a repeat of 2010, when his party won the most votes but could not find a partner that would allow it to form a government.

Any cooperation, however, would require ending a 1995 ban on government-level cooperation with the communists.

The Social Democrats will debate this controversial issue at a national congress March 15-16, and many delegates may yet shrink from any close political contact.

Should they refuse to deal with the communists, the country would most likely face an awkward coalition of left and right parties that could stall policymaking.

Easing the 1995 ban could shift Czech politics to the left, allowing a political consolidation and ending years of unstable rule in which cabinets with a shaky power base have repeatedly been unable to push through their full agenda.

Prime Minister Petr Necas, a center-right politician, has called the Communist Party "unreformable."

The communists' return has given rise to soul-searching for many beyond the ranks of party politics in the central European country of 10.5 million.

In October, the communists won spots on governing councils in the majority of the country's 13 regions in regional elections, gaining 20.4 percent of the overall vote versus 12.3 percent for Necas' Civic Democrats.

Sitting under a portrait of Karl Marx, Communist Party Deputy Chairman Jiri Dolejs said the party had apologized repeatedly for repressions of the communist era, but may yet need to make another gesture to link up with the Social Democrats.

"We are now the closest to a coalition in 23 years," he said from his office in the party's massive, nondescript headquarters, which sits on Political Prisoners Street.

For some, harsh aspects of communist rule have given way to rosier memories of a system that for all its darker aspects offered job security, cheap housing and low-cost food.

Those memories are growing stronger, especially among retirees and blue-collar workers, as the country suffers the longest recession of its post-communist history, having contracted since mid-2011 because of state spending cuts.

A January survey found that just 46 percent of people felt that today's system was better than communism, a 21-year low.

"After almost a quarter of a century, the (communist) stigmatization is not so strong anymore," said Dolejs, an economist who joined the communists in January 1989.
