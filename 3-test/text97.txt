<COUNTRY role="main">Cuba</COUNTRY> relaxes travel restrictions for citizens

New law easing strict exit visa requirements means most Cubans now eligible to leave with just passport and identity card

Cubans formed long lines outside travel agencies and migration offices in <CITY>Havana</CITY> on Monday as a new law took effect easing the island's strict exit visa requirements.

The measure means the end of both real and symbolic obstacles to travel by islanders, though it is not expected to result in a mass exodus. Most Cubans are now eligible to leave with just a current passport and national identity card, like residents of other countries.

Control over who can travel largely shifts to other governments, which will make their own decisions about granting entry visas. Cubans, like people in most other developing countries, will still find it difficult in many cases to get visas from wealthier nations, such as the <COUNTRY>US</COUNTRY>.

Several European diplomats in <CITY>Havana</CITY> said their embassies had received a high volume of calls from would-be travellers unaware they would still need a visa, despite a campaign in official Cuban media to clarify the new requirements.

"I have my passport, my identity card, everything in order," said Willian Pineira, a 23-year-old who tried to buy a plane ticket Monday to visit relatives but was turned down because he lacked an entry visa. "I wanted to go to <COUNTRY>Venezuela</COUNTRY>. But it turns out you have to have permission from them!"

There have been signs that even islanders in sensitive roles – or open opposition to the Communist government – will be included, a key litmus test of the reforms' scope. Two well-known Cuban dissidents said they were told they will be allowed to travel after being blocked in the past.

Ester Ricardo, a 68-year-old <CITY>Havana</CITY> resident, queued up early outside the office of a charter airline eager to book a flight to <CITY>Miami</CITY> as soon as possible.

"My niece invited me, so I'm going on a family visit," said Ricardo, who plans to be in <UNIT>Florida</UNIT> for around six months. "I'm not going to stay forever. I have a daughter here."

One of the first people in line at the immigration office on Monday was dissident blogger Yoani Sanchez, who says she has been denied an exit visa 20 times in recent years. Sanchez reported that her application for a new passport went smoothly. She was told it would take 15 days and once she had the document she would be able to travel. "I have hope, but I'll believe it when I'm sitting in an aeroplane," she said.

Fellow government opponent Guillermo Farinas, meanwhile, told of a surprise visit on Monday by a captain and lieutenant colonel of state security. "They said I would be able to leave the country and return," Farinas tweeted. Both he and Sanchez have said in the past that officials told them privately they would be granted permission to leave only if they agreed to forfeit their right of return.

<COUNTRY>Cuba</COUNTRY> observers and foreign governments have been waiting to see how the government implements the law to gauge its effect. The measure contains language that lets the government deny travel in cases of "national security" and one key test of the law will be whether authorities allow exits in sensitive cases, such as military officers, scientists and world-class athletes.

"We will see if this is implemented in a very open way, and if it means that all Cubans can travel," said Roberta Jacobson, <COUNTRY>US</COUNTRY> assistant secretary of state for western hemisphere affairs. "If it is implemented in such a manner, it would be a very, very positive [reform]."

In meetings throughout the country last week, doctors were told that most of them will be treated like any other citizen when it comes to travel, a surprise given <COUNTRY>Cuba</COUNTRY>'s long-standing concerns about brain drain of healthcare workers.

The Cuban law also increases the amount of time people can spend overseas without losing residency rights back home, from 11 months to two years. President Raul Castro's government apparently hopes that such extended stays for work or education will help the island in the long term, as people send money to relatives, and potentially return with saved earnings to invest in the local economy.

The Cuban exit visa has been a key point of contention for many critics of the Communist government, who seized upon the travel restrictions to call the country an island prison.

But Ileana Ros-Lehtinen, a Cuban-American Republican congresswoman from <UNIT>Florida</UNIT>, said the new travel law does not change her concerns about human rights on the island. "It's an escape valve from the regime's disastrous economic policies," said Ros-Lehtinen. "What the Cuban people want and desire is liberty and democracy."

Some analysts say the change also puts pressure on <UNIT>Washington</UNIT>'s "wet-foot, dry-foot" policy, which lets nearly all Cubans who make it to the <COUNTRY>US</COUNTRY> stay and fast-tracks them for permanent residency, and throws the spotlight on <COUNTRY>US</COUNTRY> embargo rules that bar most American travel to <COUNTRY>Cuba</COUNTRY>.

At least when it comes to crossing the <LANDMARK>Florida Straits</LANDMARK>, "<COUNTRY>Cuba</COUNTRY> now provides greater freedom of travel to virtually all of its citizens than does the <COUNTRY>US</COUNTRY>", said John McAuliff of the Fund for Reconciliation and Development, which lobbies for engagement between <UNIT>Washington</UNIT> and <COUNTRY>Cuba</COUNTRY>.