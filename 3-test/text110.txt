A quick guide to <COUNTRY role="main">Argentina</COUNTRY>

What are the highlights of a visit to the home country of the new Pope Francis? Here's what not to miss, from <CITY>Buenos Aires</CITY>' religious sites to drinking some heavenly wines in <CITY>Mendoza</CITY>

<CITY>Buenos Aires</CITY>

The reasons usually cited for visiting <CITY>Buenos Aires</CITY> are decidedly unholy: great food, fantastic wines and a sexy atmosphere. From the street art around football mecca <UNIT>La Boca</UNIT> to the cobble-stoned, increasingly gentrified streets of <UNIT>San Telmo</UNIT> and the funky shops and buzzing bars of <UNIT>Palermo Viejo</UNIT>, there is so much to excite the senses that the lack of ancient, world-class sights on a par with those of Pope Francis's new home barely registers among visitors.

There are parrillas (grills) on almost every corner – try <POI>Don Julio</POI> in <UNIT>Palermo, Soho</UNIT> (<STREET>Guatemala</STREET> <STREETNR>4691</STREETNR> and <STREET>Gurruchaga</STREET>) for a top-quality traditional experience, with excellent service from waiters who know their wine – as well as their beef. But it's not all big hunks of meat. High-end, avant garde restaurants offering new-wave Argentine cuisine (<POI>Nueva Cocina Argentina</POI>) are popping up all over the city. Among them is ultra smart <POI>Tarquino</POI> in <UNIT>Recoleta</UNIT> headed up by renowned chef, Dante Liporace, whose CV includes stints at <POI>El Bulli</POI> (<STREET>Rodríguez Peña 1967</STREET>, tarquinorestaurante.com.ar).

For religious visitors to <CITY>Buenos Aires</CITY>, the main attraction lies not in the capital itself but 70km west. The town of <CITY>Lujan</CITY>, which was founded in 1756 on the site of a shrine containing a tiny ceramic figure of the Virgin Mary, is one of the major religious centres in <REGION>Latin America</REGION>. Its epic basilica erected in honour of the Virgin of Lujan in 1887 attracts around eight million visitors a year. For a sense of the religious fervour in this small town go at the weekend when seven or eight masses are held a day, or during a pilgrimage when you'll have to jostle for space with those who've come to honour the Virgin of Lujan (on the last Sunday of September up to a million gauchos descend on the town; and on the first Sunday of October young people walk here from <CITY>Buenos Aires</CITY>).

Whether you're religious or not, one must-see is <UNIT>Recoleta</UNIT> cemetery, resting place for almost every major Argentine hero and villain, including Evita, several presidents and heavyweight boxer Luis Ángel Firpo.

The days of living like a king for a few pesos are long gone. Prices have shot up in recent years (in the past week alone, it was announced that taxi fares would soon be jumping 20%') so check out our pick of where to stay on a relative budget, how to keep your costs down and where to enjoy great food on a day trip from the city.

<CITY>Mendoza</CITY>

<CITY>Mendoza</CITY> is a must-visit for anyone, whether you're a wine lover or not. The city itself is lovely – leafy and refined with gobsmackingly good restaurants and bars on almost every street. Outside the city, vineyards stretch for miles to the foothills of the <LANDMARK>Andes</LANDMARK>, which form a constant and magnificent backdrop to the vineyards. Many of the wine makers have built architecturally stunning buildings housing high-end restaurants in the middle of rows of vines... (check out O.Fournier and Salentein). There's a wide choice of operators offering organised tours of the vineyards; if you're doing it independently, it's advisable to book in advance as many bodegas only open for pre-arranged visits.

You could easily spend a few days here working your way through the area's fantastic Malbecs but there are other activities on offer in the wildly beautiful landscape, from hiking (not many have the experience to tackle the daunting Mount <LANDMARK>Aconcagua</LANDMARK> but you can walk in its national park), and rafting to cycling and, in winter, skiing.

<LANDMARK>Iguazu</LANDMARK>

One of the natural wonders of the world, the <LANDMARK>Iguazu</LANDMARK> falls are worth more than just a day trip. They straddle the Argentine/Brazilian border, but you can get closer to the seething white water on the Argentinian side, where walkways take you right over the <LANDMARK>Garganta de Diablo</LANDMARK> "<LANDMARK>Devil's Throat</LANDMARK>", and you can also explore the forest. Once you've marvelled at the sight of 275 falls gushing over a 2.7km long precipe, spend a few days in the province of <UNIT>Misiones</UNIT>, exploring the remains of the <LANDMARK>Atlantic</LANDMARK> rainforest.

<REGION>Patagonia</REGION>

The immense wilderness of <REGION>Patagonia</REGION> offers incredible trekking in its <LANDMARK>Los Glaciares National Park</LANDMARK>, most famously on the Perito Moreno Glacier, reached from the bustling tourist hub of <CITY>El Calafate</CITY>. Then there is the village of <CITY>El Chaltén</CITY>, gateway to the <LANDMARK>Fitz Roy</LANDMARK> mountain range, whale watching at <LANDMARK>Peninsula Valdes</LANDMARK>, and the Welsh settlers' village at <CITY>Gaiman</CITY> – plus miles of untracked steppe doted with estancias. The best time to visit is Dec-March, as during the winter months of May-Oct the weather gets bitterly cold, places close and transport is less frequent.

The easiest route from <CITY>Buenos Aires</CITY> is by plane to <CITY>El Calafate</CITY> (aerolineas.com.ar), or several bus companies travel along lonely Ruta 40 – <COUNTRY>Argentina</COUNTRY>'s answer to Route 66, made famous by Ernesto 'Che' Guevara on his motorcyle tour. You can also reach the Chilean side of <REGION>Patagonia</REGION> from <CITY>El Calafate</CITY> by road to Torres Del Paine.

The north-west

The relatively unexplored yet spectacular north-west of <COUNTRY>Argentina</COUNTRY> is well worth a detour from the well-trodden route of wine country, waterfalls and glaciers. The capital <CITY>Salta</CITY> is a great base for a few days, mixing traditional gaucho and indigenous culture with sophisticated modern <COUNTRY>Argentina</COUNTRY> in the form of elegant boutique hotels and swish restaurants.

Head north of the city and you are soon into rugged mountainous scenery. The <LANDMARK>Quebrada de Humahuaca</LANDMARK>, a long and dramatic canyon of multi-hued and towering rock formations, makes for a memorable road trip ending at the tiny village of <CITY>Iruya</CITY>, where the road peters out and footpaths provide the only access into the mountains. For a sense of the scenery check out the 2007 'Domino' Guinness advert which was filmed in the village.

South of <CITY>Salta</CITY> lies the wine region of <CITY>Cafayate</CITY>, much less well known than <CITY>Mendoza</CITY> but producing some of the country's finest wines.