Is <COUNTRY role="main">India</COUNTRY> in breach of <CITY>Vienna</CITY> convention over Italian envoy?

"Habemus argumentum." It is not just because of the new Pope that <CITY>Rome</CITY> is making news in <COUNTRY>India</COUNTRY> today.

A spiralling row has broken out between <CITY>Delhi</CITY> and <CITY>Rome</CITY>, taking diplomatic rules into uncharted waters and possibly even risking Indian relations with the rest of the European Union.

Accusing <COUNTRY>Italy</COUNTRY> of "unacceptable" behaviour, <COUNTRY>India</COUNTRY> has taken the unusual step of barring its ambassador to <CITY>Delhi</CITY> from leaving, after <CITY>Rome</CITY> changed its mind on returning two Italian marines charged with murdering two fishermen off the Indian coast last year.

The Indian foreign ministry has also called in the EU envoy to <CITY>Delhi</CITY>.

Diplomatic expulsions are commonplace when relations break down between states. Not so diplomatic detentions - which conjure memories of hostage crises.

No-one is going that far yet. But others watching the row says <COUNTRY>India</COUNTRY>'s action leaves it open to the charge of breaching the <CITY>Vienna</CITY> convention which governs global diplomatic ties - potentially creating a dangerous precedent for its own envoys.

The <CITY>Vienna</CITY> text states that diplomats "shall not be liable to any form of arrest or detention".

According to one foreign embassy official in <CITY>Delhi</CITY>: "It is for diplomats themselves or their country to invoke or revoke their diplomatic protection, not the host country."

Under pressure

With an indignant Indian media at its back, and two grieving families pressing for justice, the government is under pressure to take a tough line.

It has been accused of being weak and naive in allowing the marines to go.

<COUNTRY>Italy</COUNTRY> wants the marines to be tried at home
But in effect, the government is saying <COUNTRY>Italy</COUNTRY> started the row by "breaching the norm of international law", in the words of one foreign ministry official.

Because of her Italian origins, Sonia Gandhi, the leader of <COUNTRY>India</COUNTRY>'s ruling Congress party, is also being dragged into the dispute.

It hasn't helped that the two marines were welcomed home by Italian Prime Minister Mario Monti, looking like "heroes", says former foreign secretary Kanwal Sibal.

At a briefing on Thursday, Indian foreign ministry spokesman Syed Akbaruddin rejected suggestions that <COUNTRY>India</COUNTRY> was breaching the <CITY>Vienna</CITY> convention.

Moreover, Mr Sibal says it was the Italian ambassador, Daniele Mancini, who was the first to set a precedent by giving "an affidavit to the Supreme Court" (that the marines would return). By doing so, he argues, that Mr Mancini "voluntarily subjected himself to <COUNTRY>India</COUNTRY>'s judicial process".

It is not clear whether international lawyers agree with that interpretation.

But <CITY>Rome</CITY> is refusing to send the marines back, insisting that the shooting happened in international waters and that they are not subject to Indian jurisdiction.

What may otherwise have been a nasty diplomatic spat between the two countries could still have much bigger repercussions if it is not contained.

"What is going to happen if the Italian ambassador now tries to fly out of <POI>Indira Gandhi International</POI> (<CITY>Delhi</CITY>'s main) airport?" asks one Western diplomat. "Are they going to stop him? And what happens then?"

The row could affect ongoing negotiations between <COUNTRY>India</COUNTRY> and the EU over a free-trade agreement.

<COUNTRY>Italy</COUNTRY> is already briefing fellow EU governments in <CITY>Brussels</CITY> about the case, clearly keen to press its side of the story.

But warns Mr Sibal: "If <COUNTRY>Italy</COUNTRY> cares about its long-term interests here, it will discover this was a huge error."