Sapphire discovery in <COUNTRY role="main">Madagascar</COUNTRY> sparks rush

The discovery of bountiful sapphires in a nationally-protected Madagascan rainforest has sparked a rush which conservations warn could cause untold damage.

The <LANDMARK>Ankeniheny-Zahmena</LANDMARK> corridor is made up of pristine rainforest and has been earmarked to join the Unesco World Heritage List because of its unique wildlife, including the rare Indri lemur, which featured in the 2011 Sir David Attenborough series <COUNTRY>Madagascar</COUNTRY>.

But conservationists are now warning that the discovery of a "substantial" deposit of sapphires earlier this month could see huge swathes of the remote rainforest destroyed.

As many as 10,000 miners and precious stone traders from around the world are reported to have raced to the eastern region to extract the blue-tinted stones and ship them overseas.

As well as digging up the forest floor, they are cutting down trees for firewood and shelter in the hitherto untouched wilderness and hunting resident animals, particularly lemurs, for bushmeat.

Hoteliers in <CITY>Ambatondrazaka</CITY>, the capital of <UNIT>Alaotra-Mangoro</UNIT> region where the find was recorded, say their normally quiet residences have been booked up weeks in advance.

One claimed that police and soldiers have also arrived in the area, "not to protect but too profit".

Conservationists say the government has called an emergency meeting about the issue, but lacks the security manpower to block the miners.

Rasolonirina Ramenason, the region's Environment and Forestry director, said that of the 10,000 people who have travelled to the area, around half had encroached the conservation zone.

James MacKinnon, from <COUNTRY>US</COUNTRY> charity Conservation International, said people in the precious stones industry were taking the discovery "very seriously".

"There have been around four big sapphire rushes in <COUNTRY>Madagascar</COUNTRY> but in terms of the numbers of people, this is the biggest I am aware of," he said.

Richard Hughes, Madagascan representative with the World Wide Fund for Nature, said with little or no formal protection of the rainforest, the damage would be equally significant.

"Sapphires require excavating," he said. "That will do direct damage but also for the big influx of people, there's no roads or infrastructure in place so they will be dependent on the forest for firewood, shelter and food and that will cause additional damage."