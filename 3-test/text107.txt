Aircraft crashes after crocodile on board escapes and sparks panic

A small airliner crashed into a house, killing a British pilot and 19 others after a crocodile smuggled into the aircraft in a sports bag escaped and started a panic.

The plane came down despite no apparent mechanical problems during an internal flight in the <COUNTRY>Democratic Republic of Congo</COUNTRY>.

It has now emerged that the crash was caused by the concealed reptile escaping and causing a stampede in the cabin, throwing the aircraft off-balance.

A lone survivor apparently relayed the bizarre tale to investigators.

The crocodile survived the crash, only to be dispatched with a blow from a machete.

Danny Philemotte, the Belgian pilot and 62-year-old owner of the plane's operator, Filair, struggled in vain with the controls, with Chris Wilson, his 39-year-old First Officer from <POI>Shurdington</POI>, near <CITY>Cheltenham</CITY>, <UNIT>Glocs</UNIT>.

The plane was on a routine flight from the capital, <CITY>Kinshasa</CITY>, to the regional airport at <CITY role="main">Bandundu</CITY> when the incident unfolded, on August 25.

It crashed into a house just a few hundred feet from its destination. The occupants of the property were outside at the time.

According to the inquiry report and the testimony of the only survivor, the crash happened because of a panic sparked by the escape of a crocodile hidden in a sports bag.

One of the passengers had hidden the animal, which he planned to sell, in a big sports bag, from which the reptile escaped as the plane began its descent into <CITY>Bandundu</CITY>.

A report of the incident said: "The terrified air hostess hurried towards the cockpit, followed by the passengers."

The plane was then sent off-balance "despite the desperate efforts of the pilot", said the report.

The plane was a Czech-made Let L-410 Turbolet, one of more than 1,100 produced as short-range transport aircraft and used mainly for passenger services.